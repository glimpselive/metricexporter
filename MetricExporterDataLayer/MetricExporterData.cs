﻿using MetricExporterCommon;
using MetricExporterLogger;
using MySql.Data.MySqlClient;
using Org.BouncyCastle.Crypto.Operators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterDataLayer
{
    public class MetricExporterData
    {
        private static string practiceConnectionString = "Data Source=db.glimpselive.com;Initial Catalog=Practice;Integrated Security=False;User Id=mbaDbMaster;Password=E&9u[+X,)<pG;Pooling=False";
        private static string benchmarksConnectionString = "Data Source=db.glimpselive.com;Initial Catalog=Benchmarks;Integrated Security=False;User Id=ap_benchmarks;Password=QPTQj49Gg=-Hvduj;Pooling=False";
        
            private string GetMaxBillingDateStoredProcedure = "msp_maxBillingDate";
        private string ivankaFindActiveSchemasStoredProcedure = "msp_getLocationsForMetricExporter";
        private string ivankaFindTayeLocationsStoredProcedure = "msp_getNationalTrendLocations";//"msp_getTAYELocationsForMetricExporter";
        private string ivankaFindBoALocationsStoredProcedure = "msp_getBankOfAmericaLocations";//"msp_getTAYELocationsForMetricExporter";
        private string ivankaFindAbbyLocationsStoredProcedure = "msp_getAbbyXLocations";
        private string ivankaFindAlconXLocationsStoredProcedure = "msp_getAlconXLocations";
        private string insertIntoNationalTrendStoredProcedure = "bsp_insertIntoNationalTrendMetrics";
        private string findDTPLocationsStoredProcedure = "msp_getDTPLocations";
        private string ivankaFindTotalVisionPALocationsStoredProcedure = "msp_getTotalVisionPracticeAggregateLocations";
        private static string mysqlConnectionString = "Server=" + "db1.glimpselive.com" + ";Database=common;Uid=glimpseReportGen;Pwd=gl!mp$eReportGenPassword;Connection Timeout=30";
        private string calculateStandardMetricStoredProcedure = "calculateStandardMetric";

        public List<ivankaLocationDTO> fetchActiveIvankaSchemas()
        {
            List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
            List<int> locationsList = new List<int>();

            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(ivankaFindActiveSchemasStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.legacy = false;
                                    ivankaLocationList.Add(locationInfo);
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return ivankaLocationList;
                }
            }
        }
        

             public List<ivankaLocationDTO> fetchDTPLocations()
        {
            List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
            List<int> locationsList = new List<int>();

            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(findDTPLocationsStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.state = myReader.IsDBNull(myReader.GetOrdinal("state")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("state"));
                                    locationInfo.address = myReader.IsDBNull(myReader.GetOrdinal("address")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address"));
                                    locationInfo.address2 = myReader.IsDBNull(myReader.GetOrdinal("address2")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address2"));
                                    locationInfo.city = myReader.IsDBNull(myReader.GetOrdinal("city")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("city"));
                                    locationInfo.zip = myReader.IsDBNull(myReader.GetOrdinal("zip")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("zip"));
                                    locationInfo.officePhone = myReader.IsDBNull(myReader.GetOrdinal("officePhone")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("officePhone"));
                                    locationInfo.emrName = myReader.IsDBNull(myReader.GetOrdinal("emrName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("emrName"));
                                    locationInfo.region = GetJobsonRegion(locationInfo.state);
                                    locationInfo.legacy = false;
                                    ivankaLocationList.Add(locationInfo);
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return ivankaLocationList;
                }
            }
        }
        public List<ivankaLocationDTO> fetchActiveTayeLocations()
        {
            List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
            List<int> locationsList = new List<int>();

            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(ivankaFindTayeLocationsStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.state = myReader.IsDBNull(myReader.GetOrdinal("state")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("state"));
                                    locationInfo.address = myReader.IsDBNull(myReader.GetOrdinal("address")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address"));
                                    locationInfo.address2 = myReader.IsDBNull(myReader.GetOrdinal("address2")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address2"));
                                    locationInfo.city = myReader.IsDBNull(myReader.GetOrdinal("city")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("city"));
                                    locationInfo.zip = myReader.IsDBNull(myReader.GetOrdinal("zip")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("zip"));
                                    locationInfo.officePhone = myReader.IsDBNull(myReader.GetOrdinal("officePhone")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("officePhone"));
                                    locationInfo.emrName = myReader.IsDBNull(myReader.GetOrdinal("emrName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("emrName"));
                                    locationInfo.region = GetJobsonRegion(locationInfo.state);
                                    locationInfo.legacy = false;
                                    ivankaLocationList.Add(locationInfo);
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return ivankaLocationList;
                }
            }
        }
        
            public List<ivankaLocationDTO> fetchActiveAbbyXLocations()
        {
            List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
            List<int> locationsList = new List<int>();

            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(ivankaFindAbbyLocationsStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    //sqlCommand.Parameters.Add("@extraLocations", SqlDbType.Int).Value = 0;
                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                   // locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.state = myReader.IsDBNull(myReader.GetOrdinal("state")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("state"));
                                    locationInfo.address = myReader.IsDBNull(myReader.GetOrdinal("address")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address"));
                                    locationInfo.address2 = myReader.IsDBNull(myReader.GetOrdinal("address2")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address2"));
                                    locationInfo.city = myReader.IsDBNull(myReader.GetOrdinal("city")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("city"));
                                    locationInfo.zip = myReader.IsDBNull(myReader.GetOrdinal("zip")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("zip"));
                                    locationInfo.officePhone = myReader.IsDBNull(myReader.GetOrdinal("analyzePhone")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("analyzePhone"));
                                    locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("AbbCustomerId")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("AbbCustomerId"));
                                    locationInfo.abbShipTo = myReader.IsDBNull(myReader.GetOrdinal("AbbCustomerIdWithShipTo")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("AbbCustomerIdWithShipTo"));

                                    locationInfo.emrName = myReader.IsDBNull(myReader.GetOrdinal("emrName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("emrName"));
                                    //locationInfo.region = GetJobsonRegion(locationInfo.state);
                                    locationInfo.url = myReader.IsDBNull(myReader.GetOrdinal("websiteUrl")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("websiteUrl"));
                                    locationInfo.country = myReader.IsDBNull(myReader.GetOrdinal("country")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("country"));
                                    locationInfo.program = myReader.IsDBNull(myReader.GetOrdinal("program")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("program"));
                                    locationInfo.group = myReader.IsDBNull(myReader.GetOrdinal("group")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("group"));


                                    locationInfo.accountType = myReader.IsDBNull(myReader.GetOrdinal("accountType")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("accountType"));
                                    
                                    locationInfo.legacy = false;
                                    ivankaLocationList.Add(locationInfo);
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return ivankaLocationList;
                }
            }
        }
        public List<ivankaLocationDTO> fetchActiveAlconXLocations()
        {
            List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
            List<int> locationsList = new List<int>();
            Dictionary<int, List<ivankaLocationDTO>> practiceDictionary = new Dictionary<int, List<ivankaLocationDTO>>();
            int practiceLevelCounter = 0;
            string practiceSharedBillTo = String.Empty;
            string practiceSharedBillTo2 = String.Empty;
            string practiceDPN = String.Empty;
            string practiceDPN2 = String.Empty;
            bool appendDPNToPID = false;
            string appendDPN = string.Empty;


            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(ivankaFindAlconXLocationsStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandTimeout = 180;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.active = myReader.IsDBNull(myReader.GetOrdinal("active")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("active"));
                                    locationInfo.state = myReader.IsDBNull(myReader.GetOrdinal("state")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("state"));
                                    locationInfo.address = myReader.IsDBNull(myReader.GetOrdinal("address")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address"));
                                    locationInfo.address2 = myReader.IsDBNull(myReader.GetOrdinal("address2")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address2"));
                                    locationInfo.city = myReader.IsDBNull(myReader.GetOrdinal("city")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("city"));
                                    locationInfo.zip = myReader.IsDBNull(myReader.GetOrdinal("zip")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("zip"));
                                    locationInfo.officePhone = myReader.IsDBNull(myReader.GetOrdinal("officePhone")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("officePhone"));
                                    locationInfo.SFIDShipTo = myReader.IsDBNull(myReader.GetOrdinal("SFID")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("SFID"));
                                    locationInfo.SFIDBillTo = locationInfo.SFIDShipTo;
                                    locationInfo.UCN = myReader.IsDBNull(myReader.GetOrdinal("unified_customer_number")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("unified_customer_number"));
                                    locationInfo.unified_customer_name = myReader.IsDBNull(myReader.GetOrdinal("unified_customer_name")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("unified_customer_name"));
                                    locationInfo.default_payer_number = myReader.IsDBNull(myReader.GetOrdinal("default_payer_number")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("default_payer_number"));
                                    locationInfo.default_payer_name = myReader.IsDBNull(myReader.GetOrdinal("default_payer_name")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("default_payer_name"));
                                    locationInfo.emrName = myReader.IsDBNull(myReader.GetOrdinal("emrName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("emrName"));
                                    
                                    locationInfo.region = GetJobsonRegion(locationInfo.state);
                                    locationInfo.legacy = false;
                                   
                                    if (!practiceDictionary.ContainsKey(locationInfo.practiceId))
                                    {
                                        practiceLevelCounter = 0;
                                        practiceDictionary.Add(locationInfo.practiceId, new List<ivankaLocationDTO>());
                                        practiceDictionary[locationInfo.practiceId].Add(locationInfo);
                                    
                                        
                                    }
                                    else
                                    {
                                        practiceLevelCounter += 1;
                                     
                                        practiceDictionary[locationInfo.practiceId].Add(locationInfo);


                                    }
                                    
                                }
                                myReader.Close();
                                if (SQLConn.State == ConnectionState.Open)
                                    SQLConn.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }

                    //Need to be refactor to avoid all these for loops
                    foreach (var practice in practiceDictionary)
                    {
                        practiceSharedBillTo = String.Empty;
                        
                        appendDPN = String.Empty;
                        appendDPNToPID = false;
                        foreach (var location in practice.Value)
                        {
                            
                                

                            if(appendDPN == String.Empty)
                            {
                                appendDPN = location.default_payer_number;
                            }
                            else if(appendDPN != location.default_payer_number)
                            {
                                appendDPNToPID = true;
                            }

                            if(location.UCN == location.default_payer_number && location.locationId != 11256 && location.locationId != 13801)
                            {
                                if (practiceSharedBillTo != string.Empty)
                                {
                                    //Console.WriteLine("Parent Level found - 2nd for practice");
                                    practiceSharedBillTo2 = location.SFIDShipTo;
                                    practiceDPN2 = location.default_payer_number;
                                }
                                else
                                {
                                    //Console.WriteLine("Parent Level found");
                                    practiceSharedBillTo = location.SFIDShipTo;
                                    practiceDPN = location.default_payer_number;
                                }
                            }
                        }
                        //2-16-24 - Request to Append DPN to PID to ALL locations in practice, not only the different ones
                        if (appendDPNToPID)
                        {
                            Dictionary<string,bool> dpnList = new Dictionary<string, bool>();
                            
                            //List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
                            //List<int> locationsList = new List<int>();
                            //Dictionary<int, List<ivankaLocationDTO>> practiceDictionary = new Dictionary<int, List<ivankaLocationDTO>>();

                            foreach (var location in practice.Value)
                             {
                                location.appendDPNToPID = true;
                                if(dpnList.ContainsKey(location.default_payer_number))
                                {
                                    if(!dpnList[location.default_payer_number] && location.UCN == location.default_payer_number)
                                        dpnList[location.default_payer_number] = true;
                                }
                                else
                                {

                                    dpnList.Add(location.default_payer_number, location.UCN == location.default_payer_number);
                                }
                            }
                            //3-15-24 - Request to Add parent level for appended DPN to PID accounts
                            foreach (var location in practice.Value)
                            {
                                if (!dpnList[location.default_payer_number])
                                {
                                    dpnList[location.default_payer_number] = true;
                                    location.printPracticeLevel = true;
                                }
                                    

                            }
                        }
                      
                        //ONE MORE ITERATION TO ADD TO RETURN AND IF THE EXCEPTION SCENARIO IS PRESENT TO POPULATE LOCATIONS WITH PRACTICE SFID
                        if (practiceSharedBillTo != String.Empty)
                        {
                            Console.WriteLine("Parent Level  found, setting locations SFID BILL to = Parent Level");

                            foreach (var location in practice.Value)
                            {
                                if (location.locationName.Contains("(Historic)") && location.active == 0)
                                {
                                    Logger.log(location.locationName + " not included for having Historic verbiage and being inactive");
                                    Console.WriteLine(location.locationName + " not included for having Historic verbiage and being inactive");
                                    continue;
                                }
                                if (location.default_payer_number == practiceDPN)
                                {
                                    location.SFIDBillTo = practiceSharedBillTo;
                                }
                                else if (practiceSharedBillTo2 != String.Empty && location.default_payer_number == practiceDPN2)
                                {
                                    location.SFIDBillTo = practiceSharedBillTo2;
                                }
                                ivankaLocationList.Add(location);
                            }


                        }
                        else
                        {
                            Console.WriteLine("Parent Level not found, setting locations SFID BILL to = default_payer_number");
                            if (practice.Value.Count > 1)
                            {
                                foreach (var location in practice.Value)
                                {
                                    if (location.locationName.Contains("(Historic)") && location.active == 0)
                                    {
                                        Logger.log(location.locationName + " not included for having Historic verbiage and being inactive");
                                        Console.WriteLine(location.locationName + " not included for having Historic verbiage and being inactive");
                                        continue;
                                    }
                                    if (location.locationId != 11256 && location.locationId != 13801)
                                    {

                                        Console.WriteLine("locations SFID BILL to = default_payer_number");
                                        location.SFIDBillTo = location.default_payer_number;
                                    }

                                    ivankaLocationList.Add(location);
                                }
                                ivankaLocationList.Last().printPracticeLevel = true;
                            }
                            else
                            {
                                foreach (var location in practice.Value)
                                {
                                    if (location.locationName.Contains("(Historic)") && location.active == 0)
                                    {
                                        Logger.log(location.locationName + " not included for having Historic verbiage and being inactive");
                                        Console.WriteLine(location.locationName + " not included for having Historic verbiage and being inactive");
                                        continue;
                                    }
                                    ivankaLocationList.Add(location);
                                }
                            }
                        }
                        
                       
                        
                    }

                    ////08-23-24 REMOVE ANY DUPLICATE DPN/UCN WITH ACTIVE = 0 IF THERE ARE DUPLICATES ACTIVE = 1
                    List<string> dpnActiveLocations = new List<string>();
                    List<string> locationIdsToRemove = new List<string>();
                    List<ivankaLocationDTO> activeLocations = new List<ivankaLocationDTO>();
                    //List<ivankaLocationDTO> inactiveLocations = new List<ivankaLocationDTO>();
                    activeLocations = ivankaLocationList.Where(t => t.active == 1).ToList();
                    //inactiveLocations = ivankaLocationList.Where(t => t.active != 1).ToList();

                    foreach (var location in activeLocations)
                    {
                        dpnActiveLocations.Add(location.default_payer_number);
                    }
                    foreach (var inactiveLocation in ivankaLocationList.Where(t => t.active != 1).ToList())
                    {
                        if (dpnActiveLocations.Contains(inactiveLocation.default_payer_number))
                        {
                            ivankaLocationList.Remove(inactiveLocation);
                        }

                    }
                    return ivankaLocationList;
                }
            }
        }
        public List<ivankaLocationDTO> fetchActiveBoALocations()
        {
            List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
            List<int> locationsList = new List<int>();

            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(ivankaFindBoALocationsStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    //sqlCommand.Parameters.Add("@extraLocations", SqlDbType.Int).Value = 0;
                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.state = myReader.IsDBNull(myReader.GetOrdinal("state")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("state"));
                                    locationInfo.address = myReader.IsDBNull(myReader.GetOrdinal("address")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address"));
                                    locationInfo.address2 = myReader.IsDBNull(myReader.GetOrdinal("address2")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address2"));
                                    locationInfo.city = myReader.IsDBNull(myReader.GetOrdinal("city")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("city"));
                                    locationInfo.zip = myReader.IsDBNull(myReader.GetOrdinal("zip")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("zip"));
                                    locationInfo.officePhone = myReader.IsDBNull(myReader.GetOrdinal("officePhone")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("officePhone"));
                                    locationInfo.emrName = myReader.IsDBNull(myReader.GetOrdinal("emrName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("emrName"));
                                    locationInfo.region = GetJobsonRegion(locationInfo.state);
                                    locationInfo.legacy = false;
                                    ivankaLocationList.Add(locationInfo);
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return ivankaLocationList;
                }
            }
        }
        public List<ivankaLocationDTO> fetchExtraBoALocations()
        {
            List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
            List<int> locationsList = new List<int>();

            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(ivankaFindBoALocationsStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@extraLocations", SqlDbType.Int).Value = 1;
                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.state = myReader.IsDBNull(myReader.GetOrdinal("state")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("state"));
                                    locationInfo.address = myReader.IsDBNull(myReader.GetOrdinal("address")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address"));
                                    locationInfo.address2 = myReader.IsDBNull(myReader.GetOrdinal("address2")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address2"));
                                    locationInfo.city = myReader.IsDBNull(myReader.GetOrdinal("city")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("city"));
                                    locationInfo.zip = myReader.IsDBNull(myReader.GetOrdinal("zip")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("zip"));
                                    locationInfo.officePhone = myReader.IsDBNull(myReader.GetOrdinal("officePhone")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("officePhone"));
                                    locationInfo.emrName = myReader.IsDBNull(myReader.GetOrdinal("emrName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("emrName"));
                                    locationInfo.region = GetJobsonRegion(locationInfo.state);
                                    locationInfo.legacy = false;
                                    ivankaLocationList.Add(locationInfo);
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return ivankaLocationList;
                }
            }
        }
        public List<ivankaLocationDTO> fetchActiveTotalVisionPracticeAggregateLocations()
        {
            List<ivankaLocationDTO> ivankaLocationList = new List<ivankaLocationDTO>();
            List<int> locationsList = new List<int>();

            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(ivankaFindTotalVisionPALocationsStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.state = myReader.IsDBNull(myReader.GetOrdinal("state")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("state"));
                                    locationInfo.address = myReader.IsDBNull(myReader.GetOrdinal("address")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address"));
                                    locationInfo.address2 = myReader.IsDBNull(myReader.GetOrdinal("address2")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("address2"));
                                    locationInfo.city = myReader.IsDBNull(myReader.GetOrdinal("city")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("city"));
                                    locationInfo.zip = myReader.IsDBNull(myReader.GetOrdinal("zip")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("zip"));
                                    locationInfo.officePhone = myReader.IsDBNull(myReader.GetOrdinal("officePhone")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("officePhone"));
                                    locationInfo.emrName = myReader.IsDBNull(myReader.GetOrdinal("emrName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("emrName"));
                                    locationInfo.region = GetJobsonRegion(locationInfo.state);
                                    locationInfo.legacy = false;
                                    ivankaLocationList.Add(locationInfo);
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return ivankaLocationList;
                }
            }
        }
        private string GetJobsonRegion(string state)
        {
            string regionValue = String.Empty;
            switch (state)
            {
            case "MT": regionValue = "West"; break;
            case "WY": regionValue = "West"; break;
            case "CO": regionValue = "West"; break;
            case "ID": regionValue = "West"; break;
            case "UT": regionValue = "West"; break;
            case "NV": regionValue = "West"; break;
            case "WA": regionValue = "West"; break;
            case "OR": regionValue = "West"; break;
            case "CA": regionValue = "West"; break;
            case "AK": regionValue = "West"; break;
            case "HI": regionValue = "West"; break;
            case "AZ": regionValue = "West"; break;
            case "NM": regionValue = "West"; break;

            case "TX": regionValue = "South"; break;
            case "OK": regionValue = "South"; break;
            case "AR": regionValue = "South"; break;
            case "LA": regionValue = "South"; break;
            case "MS": regionValue = "South"; break;
            case "KY": regionValue = "South"; break;
            case "TN": regionValue = "South"; break;
            case "AL": regionValue = "South"; break;
            case "GA": regionValue = "South"; break;
            case "FL": regionValue = "South"; break;
            case "SC": regionValue = "South"; break;
            case "NC": regionValue = "South"; break;
            case "VA": regionValue = "South"; break;

            case "WV": regionValue = "NorthEast"; break;
            case "MD": regionValue = "NorthEast"; break;
            case "PA": regionValue = "NorthEast"; break;
            case "NY": regionValue = "NorthEast"; break;
            case "DE": regionValue = "NorthEast"; break;
            case "NJ": regionValue = "NorthEast"; break;
            case "VT": regionValue = "NorthEast"; break;
            case "CT": regionValue = "NorthEast"; break;
            case "RI": regionValue = "NorthEast"; break;
            case "NH": regionValue = "NorthEast"; break;
            case "ME": regionValue = "NorthEast"; break;
            case "MA": regionValue = "NorthEast"; break;
            case "DC": regionValue = "NorthEast"; break;


            case "ND": regionValue = "MidWest"; break;
            case "SD": regionValue = "MidWest"; break;
            case "NE": regionValue = "MidWest"; break;
            case "KS": regionValue = "MidWest"; break;
            case "MN": regionValue = "MidWest"; break;
            case "IA": regionValue = "MidWest"; break;
            case "MO": regionValue = "MidWest"; break;
            case "WI": regionValue = "MidWest"; break;
            case "IL": regionValue = "MidWest"; break;
            case "MI": regionValue = "MidWest"; break;
            case "IN": regionValue = "MidWest"; break;
            case "OH": regionValue = "MidWest"; break;
            default: regionValue = ""; break;
            }
            return regionValue;
            
        }

        public List<ivankaLocationDTO> fetchActiveLegacySchemas(List<ivankaLocationDTO> locations)
        {


            using (MySqlConnection SQLConn = new MySqlConnection(mysqlConnectionString))
            {
                using (MySqlCommand sqlCommand = new MySqlCommand(ivankaFindActiveSchemasStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        SQLConn.Open();
                        using (MySqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    ivankaLocationDTO locationInfo = new ivankaLocationDTO();
                                    //locationInfo.abbNumber = myReader.IsDBNull(myReader.GetOrdinal("ABBNumber")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("ABBNumber"));
                                    locationInfo.locationId = myReader.IsDBNull(myReader.GetOrdinal("locationId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("locationId"));
                                    locationInfo.locationName = myReader.IsDBNull(myReader.GetOrdinal("locationName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("locationName"));
                                    locationInfo.practiceId = myReader.IsDBNull(myReader.GetOrdinal("practiceId")) ? 0 : myReader.GetInt32(myReader.GetOrdinal("practiceId"));
                                    locationInfo.practiceName = myReader.IsDBNull(myReader.GetOrdinal("practiceName")) ? string.Empty : myReader.GetString(myReader.GetOrdinal("practiceName"));
                                    locationInfo.legacy = false;
                                    locations.Add(locationInfo);
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error fetching active schemas from ivanka for automapping");
                        Logger.logException(ex);
                        Console.WriteLine("Error fetching active schemas for ivanka for automapping: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return locations;
                }
            }
            //
        }

        public void insertIntoNationalTrend(NationalTrendDTO nationalTrendDTO)
        {
            using (SqlConnection SQLConn = new SqlConnection(benchmarksConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(insertIntoNationalTrendStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@startDate", SqlDbType.Date).Value = nationalTrendDTO.startDate;
                    sqlCommand.Parameters.Add("@endDate", SqlDbType.Date).Value = nationalTrendDTO.endDate;
                    //sqlCommand.Parameters.Add("@insertedDate", SqlDbType.DateTime).Value = nationalTrendDTO.insertedDate;

                    sqlCommand.Parameters.Add("@refractions", SqlDbType.VarChar).Value = nationalTrendDTO.totalRefractions;
                    sqlCommand.Parameters.Add("@grossProduction", SqlDbType.VarChar).Value = nationalTrendDTO.totalGrossProduction ;
                    sqlCommand.Parameters.Add("@clCount", SqlDbType.VarChar).Value = nationalTrendDTO.totalClCount;
                    sqlCommand.Parameters.Add("@frameUnits", SqlDbType.VarChar).Value = nationalTrendDTO.totalFrameUnits;
                    sqlCommand.Parameters.Add("@lensCount", SqlDbType.VarChar).Value = nationalTrendDTO.totalLensCount;

                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error inserting to cachedDailyOther");
                        Logger.logException(ex);
                        Console.WriteLine("Error inserting to cachedDailyOther: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return;
                }
            }
        }
        public DateTime GetMaxBillingDate(int locationId)
        {
            DateTime maxBillDate = DateTime.MinValue;
            using (SqlConnection SQLConn = new SqlConnection(practiceConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(GetMaxBillingDateStoredProcedure, SQLConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    
                    sqlCommand.Parameters.Add("@locationId", SqlDbType.VarChar).Value = locationId.ToString();

                    try
                    {
                        SQLConn.Open();
                        using (SqlDataReader myReader = sqlCommand.ExecuteReader())
                        {
                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    maxBillDate = myReader.IsDBNull(myReader.GetOrdinal("maxDate")) ? DateTime.MinValue : myReader.GetDateTime(myReader.GetOrdinal("maxDate"));
                                    
                                }
                            }
                            myReader.Close();
                            if (SQLConn.State == ConnectionState.Open)
                                SQLConn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Error inserting to cachedDailyOther");
                        Logger.logException(ex);
                        Console.WriteLine("Error inserting to cachedDailyOther: " + ex.ToString());
                        if (SQLConn.State == ConnectionState.Open)
                            SQLConn.Close();
                    }
                    return maxBillDate;
                }
            }
        }
    }
}
