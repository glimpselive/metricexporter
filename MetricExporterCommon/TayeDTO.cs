﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon
{
    public class TayeDTO
    {
        public string grossProduction { get; set; } = String.Empty;

        public string examRevenueGross { get; set; } = String.Empty;

        public string clmaterialscurrencyGross { get; set; } = String.Empty;
        public string lensAndFramesRevenue { get; set; } = String.Empty;
        public string distinctpatientid { get; set; } = String.Empty;

        public string refractionsGross { get; set; } = String.Empty;
        public string refractions { get; set; } = String.Empty;
        public string patientsUsingInsurance { get; set; } = String.Empty;
        public string avgPatientVisits { get; set; } = String.Empty;
    }
}
