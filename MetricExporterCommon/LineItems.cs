﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon {
    public class LineItems {
        public string billingId { get; set; } = String.Empty;

        //public string sourceBillingId { get; set; } = String.Empty;

        public string billingDate { get; set; } = DateTime.MinValue.ToString();

        //public string locationId { get; set; } = String.Empty;

        public string patientId { get; set; } = String.Empty;

        public int patientIdInteger { get; set; } = 0;

        public string quantity { get; set; } = String.Empty;

        public string amount { get; set; } = String.Empty;

        public string staffId { get; set; } = String.Empty;

        public string providerId { get; set; } = String.Empty;

        //public string codeId { get; set; } = String.Empty;

        public string sourceCode1 { get; set; } = string.Empty;

        public string sourceCode2 { get; set; } = string.Empty;

        public string sourceCode3 { get; set; } = string.Empty;

        //public string transactionType { get; set; } = string.Empty;
    }
}
