﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterLogger
{
    public static class Logger {

        static string sFilePath;

        public static void createFile() {
            FileCreation();
        }

        static void FileCreation() {
            if (sFilePath == null) {
                sFilePath = AppDomain.CurrentDomain.BaseDirectory + "MetricExporterLog.txt";
            }
        }

        public static void logException(Exception ex) {
            if (sFilePath == null) {
                FileCreation();
            }
            string sDateTime = DateTime.Now.ToString();
            using (StreamWriter w = new System.IO.StreamWriter(sFilePath, true)) {
                w.WriteLine(sDateTime + " = " + ex.ToString());
            }
        }

        public static void log(String message) {
            if (sFilePath == null) {
                FileCreation();
            }
            string sDateTime = DateTime.Now.ToString();
            using (StreamWriter w = new System.IO.StreamWriter(sFilePath, true)) {
                w.WriteLine(sDateTime + " = " + message);
            }
        }

    }
}
