﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon
{
    public class NationalTrendDTO
    {

        public DateTime startDate { get; set; } = DateTime.MinValue;
        public DateTime endDate { get; set; } = DateTime.Today;
        public DateTime insertedDate { get; set; } = DateTime.Today;

        //TOTALS JOBSON
        public decimal totalRefractions { get; set; } = 0;
        public decimal totalClCount { get; set; } = 0;
        public decimal totalFrameUnits { get; set; } = 0;
        public decimal totalLensCount { get; set; } = 0;
        public decimal totalGrossProduction { get; set; } = 0;


    }
}
