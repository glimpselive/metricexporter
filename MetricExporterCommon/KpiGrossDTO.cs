﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon {
    public class KpiGrossDTO {
        public string clsoftGross { get; set; } = String.Empty;
        public string clspecialtyGross { get; set; } = String.Empty;
        public string spectacleLensSalesGross { get; set; } = String.Empty;

        public string contactLensDollarsGross { get; set; } = String.Empty;
        public string treatmentsGross { get; set; } = String.Empty;
        public string ancillaryGross { get; set; } = String.Empty;
        public string officeVisitsDollarsGross { get; set; } = String.Empty;
        public string opticalDollarsGross { get; set; } = String.Empty;
        public string vtTotalsGross { get; set; } = String.Empty;
    }
}
