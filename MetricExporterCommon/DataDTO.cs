﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon {
    public class DataDTO {
        public decimal data { get; set; } = 0;

        public string label { get; set; } = String.Empty;
    }
}
