﻿using MetricExporterCommon;
using MetricExporterDataLayer;
using MetricExporterLogger;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporter {
    class Program {
        private static MetricExporterData metricExporterData;
        static void Main(string[] args) {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            ServicePointManager.DefaultConnectionLimit = 9999;
            DateTime processFromDate = DateTime.MinValue;
            DateTime processToDate = DateTime.Now;
            int typeOfProcess = 0;
            metricExporterData = new MetricExporterData();
            Logger.createFile();
            Logger.log("Starting run.");

            try {
                processFromDate = Convert.ToDateTime(args[0]);
                processToDate = Convert.ToDateTime(args[1]);
                typeOfProcess = Convert.ToInt32(args[2]);
            } catch (Exception ex) {
                Console.WriteLine(ex);
                Logger.log("Error parsing arguments (1st= ProcessFrom,2nd=ProcessTo,3rd=ProcessType, Example: \"2022-01-01 2022-05-30 0\" ex: " + ex);
                return;
            }


            List<ivankaLocationDTO> boaLocationList = new List<ivankaLocationDTO>();
            List<ivankaLocationDTO> jobsonLocationList = new List<ivankaLocationDTO>();
            List<ivankaLocationDTO> alconXLocationList = new List<ivankaLocationDTO>();
            List<ivankaLocationDTO> abbyLocationList = new List<ivankaLocationDTO>();
            switch (typeOfProcess)
            {
                case 0:

                    Console.WriteLine("Type 0 Monday Processing for Alcon Xcel - Calculated dates(Not using provided on console args)");
                   
                    //COMMENTED OUT BOA AND JOBSON, IT IS NOW GETTING PROCESSED BY A SQL JOB
                   
                    alconXLocationList = metricExporterData.fetchActiveAlconXLocations();
                    processFromDate = new DateTime(DateTime.Now.Year, 1, 1);
                    
                    processToDate = DateTime.Today.AddDays(-1);
                    processToDate = new DateTime(processToDate.Year, processToDate.Month, processToDate.Day, 23, 59, 59);
                    if(processToDate.Month >= 4)
                    {
                        processFromDate = new DateTime(DateTime.Now.Year, 4, 1);
                    }
                    if (processToDate.Month >= 7)
                    {
                        processFromDate = new DateTime(DateTime.Now.Year, 7, 1);
                    }
                    if (processToDate.Month >= 10)
                    {
                        processFromDate = new DateTime(DateTime.Now.Year, 10, 1);
                    }
                    Console.WriteLine("Calculated Dates: processFromDate = " + processFromDate.ToString() + "processToDate = " + processToDate.ToString());                 
                    Console.WriteLine(alconXLocationList.Count.ToString() + " locations founds for alconX");
                    ProcessAlconXDP(alconXLocationList, processFromDate, processToDate, true);

                    break;
                case 1:
                    Console.WriteLine("Type 1 Processing: BoA and Jobson");
                    jobsonLocationList = metricExporterData.fetchActiveTayeLocations();
                    boaLocationList = metricExporterData.fetchActiveBoALocations();
                    Console.WriteLine(jobsonLocationList.Count.ToString() + " locations founds for jobson");
                    Console.WriteLine(boaLocationList.Count.ToString() + " locations founds for boa");
                    ProcessBankOfAmericaDP(boaLocationList, processFromDate, processToDate,false);
                    ProcessJobsonDP(jobsonLocationList, processFromDate, processToDate, true);
                    boaLocationList = metricExporterData.fetchExtraBoALocations();
                    Console.WriteLine(boaLocationList.Count.ToString() + " locations founds for boa EXTRA LOCATIONS");
                    ProcessBankOfAmericaDP(boaLocationList, processFromDate, processToDate, true);
                    break;
                case 2:
                    Console.WriteLine("Type 2 Processing: Alcon Xceleration");
                    alconXLocationList = metricExporterData.fetchActiveAlconXLocations();
                    Console.WriteLine(alconXLocationList.Count.ToString() + " locations founds for alconX");
                    processFromDate = new DateTime(DateTime.Now.Year, 1, 1);
                    processToDate = DateTime.Today.AddDays(-1);
                    ProcessAlconXDP(alconXLocationList, processFromDate, processToDate, true);
                    break;
                case 3:
                    Console.WriteLine("Type 3 Processing: Alcon Xceleration with specific dates provided on command line");
                    alconXLocationList = metricExporterData.fetchActiveAlconXLocations();
                    Console.WriteLine(alconXLocationList.Count.ToString() + " locations founds for alconX");
                    //processFromDate = new DateTime(DateTime.Now.Year, 1, 1);
                    //processToDate = DateTime.Today.AddDays(-1);
                    ProcessAlconXDP(alconXLocationList, processFromDate, processToDate, true);
                    break;
                case 4:
                    Console.WriteLine("Type 4 Processing: New Mountain Report Request");
                    abbyLocationList = metricExporterData.fetchActiveAbbyXLocations();
                    Console.WriteLine(abbyLocationList.Count.ToString() + " locations founds for Abby Report");
                    //processFromDate = new DateTime(DateTime.Now.Year, 1, 1);
                    //processToDate = DateTime.Today.AddDays(-1);
                    ProcessAbbyDP(abbyLocationList, processFromDate, processToDate);
                    break;
                
                case 5:
                    Console.WriteLine("Type 5 Processing: Verify Report Request");
                    abbyLocationList = metricExporterData.fetchActiveAbbyXLocations();
                    Console.WriteLine(abbyLocationList.Count.ToString() + " locations founds for Abby Report");
                    #region exampleDateRange
                    //processFromDate = new DateTime(DateTime.Now.Year, 1, 1);
                    //processToDate = DateTime.Today.AddDays(-1);
                    //ProcessAbbyDP(abbyLocationList, processFromDate, processToDate);
                    //List<DateTime> datesToProcess  = new List<DateTime>();
                    //datesToProcess.Add(new DateTime(2019, 1, 1));
                    //datesToProcess.Add(new DateTime(2020, 1, 1));
                    //datesToProcess.Add(new DateTime(2021, 1, 1));
                    //datesToProcess.Add(new DateTime(2022, 1, 1));
                    //datesToProcess.Add(new DateTime(2023, 1, 1));
                    //datesToProcess.Add(new DateTime(2024, 1, 1));
                    //foreach (var item in datesToProcess)
                    //{
                    //    processFromDate = item;
                    //    processToDate = new DateTime(item.Year, 12, 31,23,59,59);
                    //    ProcessVerifyDP(abbyLocationList, processFromDate, processToDate);

                    //}
                    #endregion
                    ProcessVerifyDP(abbyLocationList, processFromDate, processToDate);
                    break;
                case 6:

                    Console.WriteLine("Type 6 Quarterly Processing for Alcon Xcel - Calculated dates(Not using provided on console args)");
                    processFromDate = new DateTime(DateTime.Now.Year, 1, 1);
                    processToDate = DateTime.Today;

                    if (processToDate.Month >= 4 && processToDate.Month < 7)
                    {
                        Console.WriteLine("Printing Q1 " + DateTime.Now.Year);
                        processFromDate = new DateTime(DateTime.Now.Year, 1, 1);
                        processToDate = new DateTime(DateTime.Now.Year, 3, 31);
                    }
                    else if (processToDate.Month >= 7 && processToDate.Month < 10)
                    {
                        Console.WriteLine("Printing Q2 " + DateTime.Now.Year);
                        processFromDate = new DateTime(DateTime.Now.Year, 4, 1);
                        processToDate = new DateTime(DateTime.Now.Year, 6, 30);
                    }
                    else if (processToDate.Month >= 10)
                    {
                        Console.WriteLine("Printing Q3 " + DateTime.Now.Year);
                        processFromDate = new DateTime(DateTime.Now.Year, 7, 1);
                        processToDate = new DateTime(DateTime.Now.Year, 10, 31);
                    }else
                    {
                        Console.WriteLine("Printing Q4 " + (DateTime.Now.Year - 1).ToString());
                        processFromDate = new DateTime(DateTime.Now.Year - 1, 10, 1);
                        processToDate = new DateTime(DateTime.Now.Year - 1, 12, 31);
                    }
                    processToDate = new DateTime(processToDate.Year, processToDate.Month, processToDate.Day, 23, 59, 59);

                    Console.WriteLine("Calculated Dates: processFromDate = " + processFromDate.ToString() + " processToDate = " + processToDate.ToString());
                    alconXLocationList = metricExporterData.fetchActiveAlconXLocations();
                    Console.WriteLine(alconXLocationList.Count.ToString() + " locations founds for alconX");
                    ProcessAlconXDP(alconXLocationList, processFromDate, processToDate, true);

                    break;
                default:

                    break;
            }






            #region otherProjectsNotRelevantCurrently
            //Total Vision Process
            //locationsList = metricExporterData.fetchActiveTotalVisionPracticeAggregateLocations();
            //locationsList = metricExporterData.fetchDTPLocations();
            //ProcessAlconEQDP(alconXLocationList, processFromDate, processToDate, true);
            //ProcessAlconXDP(jobsonLocationList, processFromDate, processToDate, insertIntoNationalTrendDB);
            //ProcessClCr(locationsList, processFromDate, processToDate);
            //ProcessKPI(locationsList, processFromDate, processToDate);
            //ProcessKPI2(locationsList, processFromDate, processToDate);
            //ProcessPecca(locationsList, processFromDate, processToDate);
            //ProcessTaye(locationsList, processFromDate, processToDate);
            //ProcessDP1(locationsList, processFromDate, processToDate);
            //ProcessPracticesDP(locationsList, processFromDate, processToDate);
            //ProcessTransitions(locationsList, processFromDate, processToDate);
            //ProcessVisionSourceDP(locationsList, processFromDate, processToDate);
            //ProcessDPGL5417(locationsList, processFromDate, processToDate);

            //ProcessTotalVisionPracticeAggregate(locationsList, processFromDate, processToDate);
            //ProcessAlconMarketshare(locationsList, processFromDate, processToDate);

            //for (int i = 0; i < 9; i++)
            //{

            ///ProcessDTP(locationsList, processFromDate, processToDate);
            //ProcessVisioneeringDP(locationsList, processFromDate, processToDate, insertIntoNationalTrendDB);
            //  ProcessJobsonDP(locationsList, processFromDate, processToDate, insertIntoNationalTrendDB);
            //processFromDate = processFromDate.AddMonths(1);
            //processToDate = new DateTime(processFromDate.Year, processFromDate.Month, DateTime.DaysInMonth(processFromDate.Year, processFromDate.Month));
            ////ProcessAbbPracticeGrossInfo(locationsList, processFromDate, processToDate, insertIntoNationalTrendDB);

            //ProcessPecca2(locationsList, processFromDate, processToDate);


            //}
            //for (int i = 0; i < 2; i++)
            //{

            // ProcessCoopervisionMarketshare(locationsList, processFromDate, processToDate);
            //    processFromDate = processFromDate.AddMonths(1);
            //    processToDate = new DateTime(processFromDate.Year, processFromDate.Month, DateTime.DaysInMonth(processFromDate.Year, processFromDate.Month));
            //}
            #endregion
        }

        private static void ProcessTaye(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            foreach (var location in locationList)
            {

                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-tayeProject/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessIvankaCachedDailyUnits:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                  

                }

            }

            ExportToCSVTaye(locationList);

        }
        

        private static void ProcessVisionSourceDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-vsdp2/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    

                }
            }
            ExportToCSVVSDP2(locationList);
        }
        //ProcessVisioneeringDP
        private static void ProcessVisioneeringDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate, bool insertIntoNationalTrendDB)
        {
            int count = 0;
            NationalTrendDTO nationalTrendDTO = new NationalTrendDTO();
            nationalTrendDTO.startDate = processFromDate;
            nationalTrendDTO.endDate = processToDate;

            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-visioneeringdp1/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        //Console.WriteLine("Getting MaxBillingDate");
                        Logger.log("Getting MaxBillingDate");
                        location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                        //Console.WriteLine("MaxBillingDate Received");
                        Logger.log("MaxBillingDate Received");
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    //TRYING ONE MORE TIME
                    try
                    {
                        WebResponse responseA = requestA.GetResponse();
                        using (Stream responseStream = responseA.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                            location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                            Console.WriteLine("Processing URL: " + url);
                            Logger.log("Processing URL: " + url);
                            location.startDate = processFromDate;
                            location.endDate = processToDate;
                            //Console.WriteLine("Getting MaxBillingDate");
                            Logger.log("Getting MaxBillingDate");
                            location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                            //Console.WriteLine("MaxBillingDate Received");
                            Logger.log("MaxBillingDate Received");
                            foreach (var apiResponse in location.apiResponse)
                            {

                                MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                            }
                        }
                    }
                    catch (WebException ex2)
                    {
                        Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                        Logger.log("Exception:" + ex2.Message.ToString());
                        
                    }
                }

                count++;
            }

          
            ExportToCSVVisioneering(locationList);

        }
        private static void ProcessDTP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            int count = 0;
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-dtpanalysis/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

                count++;
            }

            ExportToCSVDTP(locationList);
        }

        private static void ProcessClCr(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            int count = 0;
            NationalTrendDTO nationalTrendDTO = new NationalTrendDTO();
            nationalTrendDTO.startDate = processFromDate;
            nationalTrendDTO.endDate = processToDate;

            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/m-contactLensCaptureRate,grossProduction/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        //Console.WriteLine("Getting MaxBillingDate");
                        Logger.log("Getting MaxBillingDate");
                        location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                        //Console.WriteLine("MaxBillingDate Received");
                        Logger.log("MaxBillingDate Received");
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    //TRYING ONE MORE TIME
                    try
                    {
                        WebResponse responseA = requestA.GetResponse();
                        using (Stream responseStream = responseA.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                            location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                            Console.WriteLine("Processing URL: " + url);
                            Logger.log("Processing URL: " + url);
                            location.startDate = processFromDate;
                            location.endDate = processToDate;
                            //Console.WriteLine("Getting MaxBillingDate");
                            Logger.log("Getting MaxBillingDate");
                            location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                            //Console.WriteLine("MaxBillingDate Received");
                            Logger.log("MaxBillingDate Received");
                            foreach (var apiResponse in location.apiResponse)
                            {

                                MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                            }
                        }
                    }
                    catch (WebException ex2)
                    {
                        Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                        Logger.log("Exception:" + ex2.Message.ToString());
                        //TRYING ONE MORE TIME
                        try
                        {
                            WebResponse responseA = requestA.GetResponse();
                            using (Stream responseStream = responseA.GetResponseStream())
                            {
                                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                                Console.WriteLine("Processing URL: " + url);
                                Logger.log("Processing URL: " + url);
                                location.startDate = processFromDate;
                                location.endDate = processToDate;
                                //Console.WriteLine("Getting MaxBillingDate");
                                Logger.log("Getting MaxBillingDate");
                                location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                                //Console.WriteLine("MaxBillingDate Received");
                                Logger.log("MaxBillingDate Received");
                                foreach (var apiResponse in location.apiResponse)
                                {

                                    MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                                }
                            }
                        }
                        catch (WebException ex3)
                        {
                            Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                            Logger.log("Exception:" + ex3.Message.ToString());
                            //TRYING ONE MORE TIME
                            try
                            {
                                WebResponse responseA = requestA.GetResponse();
                                using (Stream responseStream = responseA.GetResponseStream())
                                {
                                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                    location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                                    Console.WriteLine("Processing URL: " + url);
                                    Logger.log("Processing URL: " + url);
                                    location.startDate = processFromDate;
                                    location.endDate = processToDate;
                                    //Console.WriteLine("Getting MaxBillingDate");
                                    Logger.log("Getting MaxBillingDate");
                                    location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                                    //Console.WriteLine("MaxBillingDate Received");
                                    Logger.log("MaxBillingDate Received");
                                    foreach (var apiResponse in location.apiResponse)
                                    {

                                        MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                                    }
                                }
                            }
                            catch (WebException ex4)
                            {
                                Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                                Logger.log("Exception:" + ex4.Message.ToString());
                                //TRYING ONE MORE TIME

                            }

                        }
                    }
                }

                count++;
                // if (count > 10)
                //   break;
            }

            //ExportToCSVClCr
            ExportToCSVClCr(locationList);

        }
        private static void ProcessAlconXDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate, bool practiceLevel)
        {
            int count = 0;
            NationalTrendDTO nationalTrendDTO = new NationalTrendDTO();
            nationalTrendDTO.startDate = processFromDate;
            nationalTrendDTO.endDate = processToDate;

            //Using decimals to avoid calling practice level on every location to make processing faster and less impactful to the DB
            //Can be refactored to use practice dictionary if necessary with any request in the future
            int lastPracticeId = 0;
            string lastPracticeClmarketsharePracticeLevel = String.Empty;
            string lastPracticeClMarketshareBaselinePracticeLevel = String.Empty;

            foreach (var location in locationList)
            {
                Console.WriteLine("Processed " + count + " locations, pending:" + (locationList.Count - count).ToString());
                Logger.log("Processed " + count + " locations, pending:" + (locationList.Count - count).ToString());

                // string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-alconxdp/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/m2/m-clmarketshare,clmarketsharebaselineCached/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10259";


                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Timeout = 500000;
                    requestA.Headers["Authorization"] = "IvnakaFrontSide";
                    try
                    {
                        WebResponse responseA = requestA.GetResponse();
                        using (Stream responseStream = responseA.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                            location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                            Console.WriteLine("Processing URL: " + url);
                            Logger.log("Processing URL: " + url);
                            location.startDate = processFromDate;
                            location.endDate = processToDate;
                            location.timeframeMarketshare = location.startDate.ToString("MM/dd/yyyy") + " - " + location.endDate.ToString("MM/dd/yyyy");//"01/01/2023 - " + location.endDate.ToString("MM/dd/yyyy");
                            location.timeframeBaseline = "01/01/2024 - 12/31/2024";//"2021-01-01_2021-12-31";
                                                                                   //Console.WriteLine("Getting MaxBillingDate")
                            Logger.log("Getting MaxBillingDate");
                            location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);
                            if (location.maxBillingDate > processToDate)
                                location.maxBillingDate = processToDate;
                            //Console.WriteLine("MaxBillingDate Received");
                            Logger.log("MaxBillingDate Received");
                            foreach (var apiResponse in location.apiResponse)
                            {
                                MapToExportToCSV(apiResponse);
                            }

                        }

                        //Practice Level
                        if (lastPracticeId != location.practiceId || (lastPracticeId == location.practiceId && lastPracticeClmarketsharePracticeLevel == ""))
                        {
                            List<ivankaLocationDTO> locationIdsForPracticeLevel = locationList.Where(t => t.practiceId == location.practiceId).ToList();
                        String locationsForPracticeLevel = String.Empty;
                        int counter = 0;
                        foreach (var loc in locationIdsForPracticeLevel)
                        {
                            locationsForPracticeLevel = locationsForPracticeLevel.ToString() + loc.locationId.ToString();
                            if (counter != locationIdsForPracticeLevel.Count - 1)
                                locationsForPracticeLevel = locationsForPracticeLevel.ToString() + ", ";

                            counter++;
                        }
                        lastPracticeClmarketsharePracticeLevel = "";
                        lastPracticeClMarketshareBaselinePracticeLevel = "";
                        lastPracticeId = location.practiceId;
                            string urlPracticeLevel = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + locationsForPracticeLevel.ToString() + "/0/m2/m-clmarketshare,clmarketsharebaselineCached/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10259";



                            HttpWebRequest requestAPracticeLevel = (HttpWebRequest)WebRequest.Create(urlPracticeLevel);
                            requestAPracticeLevel.Headers["Authorization"] = "IvnakaFrontSide";
                        requestAPracticeLevel.Timeout = 500000;
                        try
                            {
                                WebResponse responseAPracticeLevel = requestAPracticeLevel.GetResponse();
                            
                            using (Stream responseStream = responseAPracticeLevel.GetResponseStream())
                                {
                                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                    location.apiResponsePracticeLevel = ParseJSONResponse(reader.ReadToEnd());
                                    Console.WriteLine("Processing Practice Level URL: " + urlPracticeLevel);
                                    Logger.log("Processing Practice Level URL: " + urlPracticeLevel);

                                    foreach (var apiResponsePracticeLevel in location.apiResponsePracticeLevel)
                                    {
                                        MapToExportToCSV(apiResponsePracticeLevel);
                                        location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketshare;
                                        location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketsharebaseline;
                                        lastPracticeClmarketsharePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketshare;
                                        lastPracticeClMarketshareBaselinePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketsharebaseline;
                                    }
                                }


                            }
                            catch (WebException ex)
                            {
                                Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                                Logger.log("Exception:" + ex.Message.ToString());
                                //System.Threading.Thread.Sleep(9000);
                                try
                                {
                                    WebResponse responseAPracticeLevel = requestAPracticeLevel.GetResponse();
                                requestAPracticeLevel.Timeout = 500000;
                                using (Stream responseStream = responseAPracticeLevel.GetResponseStream())
                                    {
                                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                        location.apiResponsePracticeLevel = ParseJSONResponse(reader.ReadToEnd());
                                        Console.WriteLine("Processing Practice Level URL: " + urlPracticeLevel);
                                        Logger.log("Processing Practice Level URL: " + urlPracticeLevel);

                                        foreach (var apiResponsePracticeLevel in location.apiResponsePracticeLevel)
                                        {
                                            MapToExportToCSV(apiResponsePracticeLevel);
                                            location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketshare;
                                            location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketsharebaseline;
                                            lastPracticeClmarketsharePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketshare;
                                            lastPracticeClMarketshareBaselinePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketsharebaseline;
                                        }
                                    }


                                }
                                catch (WebException ex2)
                                {
                                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                                    Logger.log("Exception:" + ex2.Message.ToString());

                                    //TRYING ONE MORE TIME


                                }
                                //TRYING ONE MORE TIME


                            }
                        }
                        else
                        {
                            //USE LAST REQUEST DATA
                            location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel = lastPracticeClmarketsharePracticeLevel;
                            location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel = lastPracticeClMarketshareBaselinePracticeLevel;

                        }
                    }
                    catch (WebException ex3)
                    {
                        Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                        Logger.log("Exception:" + ex3.Message.ToString());
                        System.Threading.Thread.Sleep(9000);
                        //TRYING ONE MORE TIME
                        try
                        {
                            WebResponse responseA = requestA.GetResponse();
                            using (Stream responseStream = responseA.GetResponseStream())
                            {
                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                            location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                            Console.WriteLine("Processing URL: " + url);
                            Logger.log("Processing URL: " + url);
                            location.startDate = processFromDate;
                            location.endDate = processToDate;
                            location.timeframeMarketshare = location.endDate.ToString("MM/dd/yyyy") + " - " + location.endDate.ToString("MM/dd/yyyy");//"01/01/2023 - " + location.endDate.ToString("MM/dd/yyyy");
                            location.timeframeBaseline = "01/01/2024 - 12/31/2024";//"2021-01-01_2021-12-31";
                                                                                   //Console.WriteLine("Getting MaxBillingDate")
                            Logger.log("Getting MaxBillingDate");
                            location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);
                            if (location.maxBillingDate > processToDate)
                                location.maxBillingDate = processToDate;
                            //Console.WriteLine("MaxBillingDate Received");
                            Logger.log("MaxBillingDate Received");
                            foreach (var apiResponse in location.apiResponse)
                            {
                                MapToExportToCSV(apiResponse);
                            }

                        }

                            //Practice Level
                            if (lastPracticeId != location.practiceId)
                            {
                                lastPracticeId = location.practiceId;

                            List<ivankaLocationDTO> locationIdsForPracticeLevel = locationList.Where(t => t.practiceId == location.practiceId).ToList();
                            String locationsForPracticeLevel = String.Empty;
                            int counter = 0;
                            foreach (var loc in locationIdsForPracticeLevel)
                            {
                                locationsForPracticeLevel = locationsForPracticeLevel.ToString() + loc.locationId.ToString();
                                if (counter != locationIdsForPracticeLevel.Count - 1)
                                    locationsForPracticeLevel = locationsForPracticeLevel.ToString() + ", ";

                                counter++;
                            }
                            //string urlPracticeLevel = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/0/0/m2/m-clmarketshare,clmarketshareBaseline/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10259";
                            string urlPracticeLevel = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + locationsForPracticeLevel.ToString() + "/0/m2/m-clmarketshare,clmarketsharebaselineCached/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10259";



                            HttpWebRequest requestAPracticeLevel = (HttpWebRequest)WebRequest.Create(urlPracticeLevel);
                                requestAPracticeLevel.Headers["Authorization"] = "IvnakaFrontSide";

                                try
                                {
                                    WebResponse responseAPracticeLevel = requestAPracticeLevel.GetResponse();
                                    using (Stream responseStream = responseAPracticeLevel.GetResponseStream())
                                    {
                                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                        location.apiResponsePracticeLevel = ParseJSONResponse(reader.ReadToEnd());
                                        Console.WriteLine("Processing Practice Level URL: " + urlPracticeLevel);
                                        Logger.log("Processing Practice Level URL: " + urlPracticeLevel);

                                        foreach (var apiResponsePracticeLevel in location.apiResponsePracticeLevel)
                                        {
                                            MapToExportToCSV(apiResponsePracticeLevel);
                                            location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketshare;
                                            location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketsharebaseline;
                                            lastPracticeClmarketsharePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketshare;
                                            lastPracticeClMarketshareBaselinePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketsharebaseline;
                                        }
                                    }


                                }
                                catch (WebException ex4)
                                {
                                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                                    Logger.log("Exception:" + ex4.Message.ToString());
                                    System.Threading.Thread.Sleep(9000);
                                    try
                                    {
                                        WebResponse responseAPracticeLevel = requestAPracticeLevel.GetResponse();
                                        using (Stream responseStream = responseAPracticeLevel.GetResponseStream())
                                        {
                                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                            location.apiResponsePracticeLevel = ParseJSONResponse(reader.ReadToEnd());
                                            Console.WriteLine("Processing Practice Level URL: " + urlPracticeLevel);
                                            Logger.log("Processing Practice Level URL: " + urlPracticeLevel);

                                            foreach (var apiResponsePracticeLevel in location.apiResponsePracticeLevel)
                                            {
                                                MapToExportToCSV(apiResponsePracticeLevel);
                                                location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketshare;
                                                location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketsharebaseline;
                                                lastPracticeClmarketsharePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketshare;
                                                lastPracticeClMarketshareBaselinePracticeLevel = location.apiResponsePracticeLevel[0].dp1DTO.clmarketsharebaseline;
                                            }
                                        }


                                    }
                                    catch (WebException ex5)
                                    {
                                        Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                                        Logger.log("Exception:" + ex5.Message.ToString());

                                        //TRYING ONE MORE TIME


                                    }
                                    //TRYING ONE MORE TIME


                                }
                            }
                            else
                            {
                                //USE LAST REQUEST DATA
                                location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel = lastPracticeClmarketsharePracticeLevel;
                                location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel = lastPracticeClMarketshareBaselinePracticeLevel;

                            }
                        }
                        catch (WebException ex)
                        {
                            Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                            Logger.log("Exception:" + ex.Message.ToString());
                            //TRYING ONE MORE TIME




                        }



                    }
               
                count++;
                // if (count > 10)
                //   break;
            }

            //if (insertIntoNationalTrendDB)
            //{
            //    metricExporterData.insertIntoNationalTrend(nationalTrendDTO);
            //}
            ExportToCSVAlconXceleration(locationList, practiceLevel);
            ExportToCSVAlconXceleration2(locationList, practiceLevel);
            //ExportToCSVJobsonTemp(locationList);
        }
        private static void ProcessAlconEQDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate, bool practiceLevel)
        {
            int count = 0;
            NationalTrendDTO nationalTrendDTO = new NationalTrendDTO();
            nationalTrendDTO.startDate = processFromDate;
            nationalTrendDTO.endDate = processToDate;

            foreach (var location in locationList)
            {
               // string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-alconxdp/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/m2/g-alconxdp/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10259";
                //if(practiceLevel)
                 //   url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/0/0/m2/m-clmarketshare,clmarketshareBaseline/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10259";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        //Console.WriteLine("Getting MaxBillingDate");
                        Logger.log("Getting MaxBillingDate");
                        location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                        //Console.WriteLine("MaxBillingDate Received");
                        Logger.log("MaxBillingDate Received");
                        foreach (var apiResponse in location.apiResponse)
                        {
                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    //TRYING ONE MORE TIME
                    try
                    {
                        WebResponse responseA = requestA.GetResponse();
                        using (Stream responseStream = responseA.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                            location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                            Console.WriteLine("Processing URL: " + url);
                            Logger.log("Processing URL: " + url);
                            location.startDate = processFromDate;
                            location.endDate = processToDate;
                            //Console.WriteLine("Getting MaxBillingDate");
                            Logger.log("Getting MaxBillingDate");
                            location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                            //Console.WriteLine("MaxBillingDate Received");
                            Logger.log("MaxBillingDate Received");
                            foreach (var apiResponse in location.apiResponse)
                            {

                                MapToExportToCSV(apiResponse);
                            }
                        }
                    }
                    catch (WebException ex2)
                    {
                        Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                        Logger.log("Exception:" + ex2.Message.ToString());
                        //TRYING ONE MORE TIME
                        try
                        {
                            WebResponse responseA = requestA.GetResponse();
                            using (Stream responseStream = responseA.GetResponseStream())
                            {
                                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                                Console.WriteLine("Processing URL: " + url);
                                Logger.log("Processing URL: " + url);
                                location.startDate = processFromDate;
                                location.endDate = processToDate;
                                //Console.WriteLine("Getting MaxBillingDate");
                                Logger.log("Getting MaxBillingDate");
                                location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                                //Console.WriteLine("MaxBillingDate Received");
                                Logger.log("MaxBillingDate Received");
                                foreach (var apiResponse in location.apiResponse)
                                {

                                    MapToExportToCSV(apiResponse);
                                }
                            }
                        }
                        catch (WebException ex3)
                        {
                            Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                            Logger.log("Exception:" + ex3.Message.ToString());
                            //TRYING ONE MORE TIME
                            try
                            {
                                WebResponse responseA = requestA.GetResponse();
                                using (Stream responseStream = responseA.GetResponseStream())
                                {
                                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                    location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                                    Console.WriteLine("Processing URL: " + url);
                                    Logger.log("Processing URL: " + url);
                                    location.startDate = processFromDate;
                                    location.endDate = processToDate;
                                    //Console.WriteLine("Getting MaxBillingDate");
                                    Logger.log("Getting MaxBillingDate");
                                    location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                                    //Console.WriteLine("MaxBillingDate Received");
                                    Logger.log("MaxBillingDate Received");
                                    foreach (var apiResponse in location.apiResponse)
                                    {

                                        MapToExportToCSV(apiResponse);
                                    }
                                }
                            }
                            catch (WebException ex4)
                            {
                                Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                                Logger.log("Exception:" + ex4.Message.ToString());
                                //TRYING ONE MORE TIME

                            }

                        }
                    }
                }

                count++;
                // if (count > 10)
                //   break;
            }

            //if (insertIntoNationalTrendDB)
            //{
            //    metricExporterData.insertIntoNationalTrend(nationalTrendDTO);
            //}
            ExportToCSVAlconX(locationList);
            //ExportToCSVJobsonTemp(locationList);
        }

        private static void ProcessJobsonDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate, bool insertIntoNationalTrendDB)
        {
            int count = 0;
            NationalTrendDTO nationalTrendDTO = new NationalTrendDTO();
            nationalTrendDTO.startDate = processFromDate;
            nationalTrendDTO.endDate = processToDate;

            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-jobsondp1/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";
               
                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        //Console.WriteLine("Getting MaxBillingDate");
                        Logger.log("Getting MaxBillingDate");
                        location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                        //Console.WriteLine("MaxBillingDate Received");
                        Logger.log("MaxBillingDate Received");
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    //TRYING ONE MORE TIME
                    try
                    {
                        WebResponse responseA = requestA.GetResponse();
                        using (Stream responseStream = responseA.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                            location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                            Console.WriteLine("Processing URL: " + url);
                            Logger.log("Processing URL: " + url);
                            location.startDate = processFromDate;
                            location.endDate = processToDate;
                            //Console.WriteLine("Getting MaxBillingDate");
                            Logger.log("Getting MaxBillingDate");
                            location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                            //Console.WriteLine("MaxBillingDate Received");
                            Logger.log("MaxBillingDate Received");
                            foreach (var apiResponse in location.apiResponse)
                            {

                                MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                            }
                        }
                    }
                    catch (WebException ex2)
                    {
                        Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                        Logger.log("Exception:" + ex2.Message.ToString());
                        //TRYING ONE MORE TIME
                        try
                        {
                            WebResponse responseA = requestA.GetResponse();
                            using (Stream responseStream = responseA.GetResponseStream())
                            {
                                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                                Console.WriteLine("Processing URL: " + url);
                                Logger.log("Processing URL: " + url);
                                location.startDate = processFromDate;
                                location.endDate = processToDate;
                                //Console.WriteLine("Getting MaxBillingDate");
                                Logger.log("Getting MaxBillingDate");
                                location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                                //Console.WriteLine("MaxBillingDate Received");
                                Logger.log("MaxBillingDate Received");
                                foreach (var apiResponse in location.apiResponse)
                                {

                                    MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                                }
                            }
                        }
                        catch (WebException ex3)
                        {
                            Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                            Logger.log("Exception:" + ex3.Message.ToString());
                            //TRYING ONE MORE TIME
                            try
                            {
                                WebResponse responseA = requestA.GetResponse();
                                using (Stream responseStream = responseA.GetResponseStream())
                                {
                                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                    location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                                    Console.WriteLine("Processing URL: " + url);
                                    Logger.log("Processing URL: " + url);
                                    location.startDate = processFromDate;
                                    location.endDate = processToDate;
                                    //Console.WriteLine("Getting MaxBillingDate");
                                    Logger.log("Getting MaxBillingDate");
                                    location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                                    //Console.WriteLine("MaxBillingDate Received");
                                    Logger.log("MaxBillingDate Received");
                                    foreach (var apiResponse in location.apiResponse)
                                    {

                                        MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                                    }
                                }
                            }
                            catch (WebException ex4)
                            {
                                Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                                Logger.log("Exception:" + ex4.Message.ToString());
                                //TRYING ONE MORE TIME

                            }

                        }
                    }
                }

                count++;
                // if (count > 10)
                //   break;
            }
            
            if (insertIntoNationalTrendDB)
            {
                metricExporterData.insertIntoNationalTrend(nationalTrendDTO);
            }
            ExportToCSVJobson(locationList);
            //ExportToCSVJobsonTemp(locationList);
        }
        private static void ProcessAlconDashboardDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate, bool insertIntoNationalTrendDB)
        {
            int count = 0;
            NationalTrendDTO nationalTrendDTO = new NationalTrendDTO();
            nationalTrendDTO.startDate = processFromDate;
            nationalTrendDTO.endDate = processToDate;

            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-jobsondp1/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                ServicePointManager.DefaultConnectionLimit = 9999;
                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        //Console.WriteLine("Getting MaxBillingDate");
                        Logger.log("Getting MaxBillingDate");
                        location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                        //Console.WriteLine("MaxBillingDate Received");
                        Logger.log("MaxBillingDate Received");
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    //TRYING ONE MORE TIME
                    try
                    {
                        WebResponse responseA = requestA.GetResponse();
                        using (Stream responseStream = responseA.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                            location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                            Console.WriteLine("Processing URL: " + url);
                            Logger.log("Processing URL: " + url);
                            location.startDate = processFromDate;
                            location.endDate = processToDate;
                            //Console.WriteLine("Getting MaxBillingDate");
                            Logger.log("Getting MaxBillingDate");
                            location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                            //Console.WriteLine("MaxBillingDate Received");
                            Logger.log("MaxBillingDate Received");
                            foreach (var apiResponse in location.apiResponse)
                            {

                                MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                            }
                        }
                    }
                    catch (WebException ex2)
                    {
                        Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                        Logger.log("Exception:" + ex2.Message.ToString());
                        //TRYING ONE MORE TIME
                        try
                        {
                            WebResponse responseA = requestA.GetResponse();
                            using (Stream responseStream = responseA.GetResponseStream())
                            {
                                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                                Console.WriteLine("Processing URL: " + url);
                                Logger.log("Processing URL: " + url);
                                location.startDate = processFromDate;
                                location.endDate = processToDate;
                                //Console.WriteLine("Getting MaxBillingDate");
                                Logger.log("Getting MaxBillingDate");
                                location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                                //Console.WriteLine("MaxBillingDate Received");
                                Logger.log("MaxBillingDate Received");
                                foreach (var apiResponse in location.apiResponse)
                                {

                                    MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                                }
                            }
                        }
                        catch (WebException ex3)
                        {
                            Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                            Logger.log("Exception:" + ex3.Message.ToString());
                            //TRYING ONE MORE TIME
                            try
                            {
                                WebResponse responseA = requestA.GetResponse();
                                using (Stream responseStream = responseA.GetResponseStream())
                                {
                                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                                    location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                                    Console.WriteLine("Processing URL: " + url);
                                    Logger.log("Processing URL: " + url);
                                    location.startDate = processFromDate;
                                    location.endDate = processToDate;
                                    //Console.WriteLine("Getting MaxBillingDate");
                                    Logger.log("Getting MaxBillingDate");
                                    location.maxBillingDate = metricExporterData.GetMaxBillingDate(location.locationId);

                                    //Console.WriteLine("MaxBillingDate Received");
                                    Logger.log("MaxBillingDate Received");
                                    foreach (var apiResponse in location.apiResponse)
                                    {

                                        MapToExportToCSVJobson(apiResponse, nationalTrendDTO);
                                    }
                                }
                            }
                            catch (WebException ex4)
                            {
                                Logger.log("Error Processing 2nd link in ProcessDP1:" + url.ToString());
                                Logger.log("Exception:" + ex4.Message.ToString());
                                //TRYING ONE MORE TIME

                            }

                        }
                    }
                }

                count++;
                // if (count > 10)
                //   break;
            }

            if (insertIntoNationalTrendDB)
            {
                metricExporterData.insertIntoNationalTrend(nationalTrendDTO);
            }
            ExportToCSVJobson(locationList);
            //ExportToCSVJobsonTemp(locationList);
        }
        private static void ProcessCoopervisionMarketshare(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            int count = 0;
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/m1/m-clmarketshare/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10259";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());

                }
                count++;
            }

            ExportToCooperCLMarketshare(locationList);
        }
        private static void ProcessDP1(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            int count = 0;
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-dp4/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/0";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    
                }
                count++;
            }
            
            ExportToCSVDP4(locationList);
        }
        private static void ProcessPecca2(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-dp3/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

            }

            ExportToCSVPecca2(locationList);
        }
        private static void ProcessBankOfAmericaDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate,bool extraLocations)
        {
            int count = 0;
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/m-grossProduction/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/0";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                ServicePointManager.DefaultConnectionLimit = 9999;
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

                count++;
            }

            ExportToCSVBoA(locationList, extraLocations);
        }
        private static void ProcessAbbyDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            int count = 0;
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/0/g-abbnmreq1/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10111";
                Console.WriteLine("Processed " + count + " locations, pending:" + (locationList.Count - count).ToString());
                Logger.log("Processed " + count + " locations, pending:" + (locationList.Count - count).ToString());

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                ServicePointManager.DefaultConnectionLimit = 9999;
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

                count++;
            }

            ExportToCSVAbby(locationList);
        }
        private static void ProcessVerifyDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            int count = 0;
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/0/g-abbverifyreq1/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1/10111";
                Console.WriteLine("Processed " + count + " locations, pending:" + (locationList.Count - count).ToString());
                Logger.log("Processed " + count + " locations, pending:" + (locationList.Count - count).ToString());

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                ServicePointManager.DefaultConnectionLimit = 9999;
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

                count++;
            }

            ExportToCSVVerify(locationList);
           // ExportToCSVTotalVision(locationList);
        }
        private static void ProcessTotalVisionPracticeAggregate(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-VisionCouncilPracticeAggregate/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

            }

            ExportToVCPA(locationList);

        }
        private static void ProcessAlconMarketshare(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            int count = 0;
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/m-clmarketshare/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                   
                }

                count++;
            }
            ExportToCooperCLMarketshare(locationList);

        }
        private static void ProcessCitadelDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-citadelDP/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/0";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

            }

            ExportToCSVCitadel(locationList);
        }

        private static void ProcessDPGL5417(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-DP-GL-5417/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

            }
            ExportToCSVDPGL5417(locationList);
        }
        private static void ExportToCSVDPGL5417(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Data-Export5417_v1" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20}", "Practice Id", "Practice Name", "Location Id", "Location Name", "State", "Region",   "CL Revenue Gross", "Previous CL Revenue Gross", "CL Revenue Growth", "CL Fit Revenue Gross", "Previous CL Fit Revenue Gross", "CL Fit Revenue Growth", "CL Fit Units", "Previous CL Fit Units", "CL Fit Units Growth", "Revenue Per Patient", "Previous Revenue Per Patient", "Revenue Per Patient Growth", "Annual Supply", "Previous Annual Supply", "Annual Supply Growth");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",\"{4}\",\"{5}\",{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20}", location.practiceId, location.practiceName, location.locationId, location.locationName, location.state, GetRegion(location.state), location.apiResponse[0].dp1DTO.contactLensDollarsGross, location.apiResponse[0].dp1DTO.prevContactLensDollarsGross, location.apiResponse[0].dp1DTO.contactLensDollarsGrossGrowth, location.apiResponse[0].dp1DTO.clfittingscurrencyGross, location.apiResponse[0].dp1DTO.prevClfittingscurrencyGross, location.apiResponse[0].dp1DTO.clfittingscurrencyGrossGrowth, location.apiResponse[0].dp1DTO.contactLensFittings, location.apiResponse[0].dp1DTO.prevContactLensFittings, location.apiResponse[0].dp1DTO.contactLensFittingsGrowth, location.apiResponse[0].dp1DTO.revenuePerPatientGross, location.apiResponse[0].dp1DTO.prevRevenuePerPatientGross, location.apiResponse[0].dp1DTO.revenuePerPatientGrossGrowth, location.apiResponse[0].dp1DTO.clAnnualSupplyPct, location.apiResponse[0].dp1DTO.prevClAnnualSupplyPct, location.apiResponse[0].dp1DTO.ClAnnualSupplyPctGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVCitadel(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Citadel_v1_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6}", "Practice Id", "Practice Name", "Location Id", "Location Name", "State", "Region", "Gross Production");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",\"{4}\",\"{5}\",{6}", location.practiceId, location.practiceName, location.locationId, location.locationName, location.state, GetRegion(location.state), location.apiResponse[0].dp1DTO.grossProduction);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ProcessTransitions(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            int count = 0;
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-transitionsdp/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/0";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in transitionsdp:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

                count++;
            }

            ExportToCSVTransitions(locationList);
        }
        private static void ProcessPracticesDP(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-AbbPracticesDP1/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/0";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessDP1:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                }

            }

            ExportToCSVDPPractices(locationList);

        }
        private static void ProcessPecca(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate)
        {
            foreach (var location in locationList)
            {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-peccaMetricExport/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/1";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try
                {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse)
                        {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                }
                catch (WebException ex)
                {
                    Logger.log("Error Processing link in ProcessIvankaCachedDailyUnits:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    WebResponse errorResponse = ex.Response;
                    using (Stream responseStream = errorResponse.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                        String errorText = reader.ReadToEnd();
                        // log errorText
                    }

                }
            }

            ExportToCSVPecca(locationList);

        }
        private static void ProcessKPI(List<ivankaLocationDTO> locationList,DateTime processFromDate, DateTime processToDate) {
            foreach (var location in locationList) {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-kpiservicesummarygross/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/0";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream()) {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                           location.startDate = processFromDate;
                            location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse) {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                } catch (WebException ex) {
                    Logger.log("Error Processing link in ProcessIvankaCachedDailyUnits:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    WebResponse errorResponse = ex.Response;
                    using (Stream responseStream = errorResponse.GetResponseStream()) {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                        String errorText = reader.ReadToEnd();
                    }
                }
            }
            ExportToCSV(locationList);
            
        }
        private static void ProcessKPI2(List<ivankaLocationDTO> locationList, DateTime processFromDate, DateTime processToDate) {
            foreach (var location in locationList) {
                string url = "https://api.glimpselive.com/api/metrics/" + location.practiceId.ToString() + "/" + location.locationId.ToString() + "/0/g-kpicontactlensspecialtygross/0/" + processFromDate.ToString("yyyy-MM-dd") + "/" + processToDate.ToString("yyyy-MM-dd") + "/0/0/0";

                HttpWebRequest requestA = (HttpWebRequest)WebRequest.Create(url);
                requestA.Headers["Authorization"] = "IvnakaFrontSide";
                try {
                    WebResponse responseA = requestA.GetResponse();
                    using (Stream responseStream = responseA.GetResponseStream()) {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        location.apiResponse = ParseJSONResponse(reader.ReadToEnd());
                        Console.WriteLine("Processing URL: " + url);
                        Logger.log("Processing URL: " + url);
                        location.startDate = processFromDate;
                        location.endDate = processToDate;
                        foreach (var apiResponse in location.apiResponse) {

                            MapToExportToCSV(apiResponse);
                        }
                    }
                } catch (WebException ex) {
                    Logger.log("Error Processing link in ProcessIvankaCachedDailyUnits:" + url.ToString());
                    Logger.log("Exception:" + ex.Message.ToString());
                    WebResponse errorResponse = ex.Response;
                    using (Stream responseStream = errorResponse.GetResponseStream()) {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                        String errorText = reader.ReadToEnd();
                    }

                }
            }

            ExportToCSV2(locationList);

        }
        private static void ExportToCSV(List<ivankaLocationDTO> locations) {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\ABB_KPI_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3}", "GlimpseId", "contactLensDollarsGross", "officeVisitsDollarsGross", "opticalDollarsGross");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations) {
                if (location.apiResponse.Count > 0) {
                    var newLineT = string.Format("{0},{1},{2},{3}", location.locationId, location.apiResponse[0].KpiGross.contactLensDollarsGross.Trim(), location.apiResponse[0].KpiGross.officeVisitsDollarsGross.Trim(), location.apiResponse[0].KpiGross.opticalDollarsGross.Trim());
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVPecca2(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\PeccaExp_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "Practice Name", "Location Name", "Cl Capture Rate 2019", "Cl Capture Rate 2020", "Annual Supply Pct 2019", "Annual Supply Pct 2020", "Gross Production 2019", "Gross Production 2020");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("\"{0}\",\"{1}\",{2},{3},{4},{5},{6},{7}", location.practiceName, location.locationName, location.apiResponse[0].dp1DTO.prevContactLensCaptureRate, location.apiResponse[0].dp1DTO.contactLensCaptureRate, location.apiResponse[0].dp1DTO.prevClAnnualSupplyPct.Trim(), location.apiResponse[0].dp1DTO.clAnnualSupplyPct.Trim(), location.apiResponse[0].dp1DTO.prevGrossProduction.Trim(),location.apiResponse[0].dp1DTO.grossProduction.Trim());
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVPecca(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Pecca_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4}", "Practice Name", "Location Name", "Multifocal Growth", "Multifocal Pct", "Rev per CL Fitting");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("\"{0}\",\"{1}\",{2},{3},{4}", location.practiceName, location.locationName, location.apiResponse[0].peccaExportDTO.clmultifocalGrowth, location.apiResponse[0].peccaExportDTO.clmultifocalpct.Trim(), location.apiResponse[0].peccaExportDTO.revenuePerCLFittingGross.Trim());
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVTransitions(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Transitions_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", "Practice Id", "Practice Name", "Location Id", "Location Name", "City", "State", "Phone", "Gross Production", "Photochromatic", "PhotochromaticCR", "Optical Dollars","Lens Count");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",\"{4}\",\"{5}\",\"{6}\",{7},{8},{9},{10},{11}", location.practiceId, location.practiceName, location.locationId, location.locationName, location.city, location.state, location.officePhone, location.apiResponse[0].dp1DTO.grossProduction, location.apiResponse[0].dp1DTO.photochromatic, location.apiResponse[0].dp1DTO.photochromaticCR, location.apiResponse[0].dp1DTO.opticalDollarsGross, location.apiResponse[0].dp1DTO.lensCount);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVDP4(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Affinity_Export2_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "Practice Name","Location Id", "Location Name","City", "State", "Phone", "Gross Production", "Total Capture Rate");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("\"{0}\",{1},\"{2}\",\"{3}\",\"{4}\",\"{5}\",{6},{7}", location.practiceName, location.locationId,location.locationName,location.city,location.state,location.officePhone, location.apiResponse[0].dp1DTO.grossProduction, location.apiResponse[0].dp1DTO.totalCaptureRate.Trim());
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        
        private static void ExportToVCPA(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\TotalVision_PAExport_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24}", "Practice Id", "Practice Name", "Location Id", "Location Name", "Gross Production", "Prev Gross Production", "Gross Production Growth", "distinctpatientid", "Previous distinctpatientid", "Growth distinctpatientid", "refractions", "Previous refractions", "Growth  refractions", "conversionRate", "Previous conversionRate", "Growth conversionRate",
 "clAnnualSupplyPct", "Previous clAnnualSupplyPct", "Growth clAnnualSupplyPct", "fundusScreeningsCR", "Previous fundusScreeningsCR", "Growth fundusScreeningsCR", "arCaptureRate", "Previous arCaptureRate", "Growth arCaptureRate",
 "existingPatients", "Previous existingPatients", "Growth existingPatients", "newPatients", "Previous newPatients", "Growth newPatients",
 "revenuePerEyewearPairGross", "Previous revenuePerEyewearPairGross", "Growth revenuePerEyewearPairGross", "revenuePerPatientGross", "Previous revenuePerPatientGross", "Growth revenuePerPatientGross",
 "multiPairPct", "Previous multiPairPct", "Growth multiPairPct", "payment", "Previous payment", "Growth payment", "patientPayments", "Previous patientPayments", "Growth patientPayments", "revenuePerCLFittingGross", "Previous revenuePerCLFittingGross", "Growth revenuePerCLFittingGross");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24}", location.practiceId, location.practiceName, location.locationId, location.locationName, location.apiResponse[0].dp1DTO.grossProduction, location.apiResponse[0].dp1DTO.prevGrossProduction, location.apiResponse[0].dp1DTO.grossProductionGrowth, location.apiResponse[0].dp1DTO.distinctpatientid, location.apiResponse[0].dp1DTO.prevDistinctpatientid, location.apiResponse[0].dp1DTO.distinctpatientidGrowth, location.apiResponse[0].dp1DTO.refractions, location.apiResponse[0].dp1DTO.prevRefractions, location.apiResponse[0].dp1DTO.refractionsGrowth, location.apiResponse[0].dp1DTO.conversionRate, location.apiResponse[0].dp1DTO.prevConversionRate, location.apiResponse[0].dp1DTO.conversionRateGrowth, location.apiResponse[0].dp1DTO.clAnnualSupplyPct, location.apiResponse[0].dp1DTO.prevClAnnualSupplyPct, location.apiResponse[0].dp1DTO.ClAnnualSupplyPctGrowth, location.apiResponse[0].dp1DTO.fundusScreeningsCR, location.apiResponse[0].dp1DTO.prevFundusScreeningsCR, location.apiResponse[0].dp1DTO.fundusScreeningsCRGrowth, location.apiResponse[0].dp1DTO.arCaptureRate, location.apiResponse[0].dp1DTO.prevArCaptureRate, location.apiResponse[0].dp1DTO.arCaptureRateGrowth, location.apiResponse[0].dp1DTO.existingPatients, location.apiResponse[0].dp1DTO.prevExistingPatients, location.apiResponse[0].dp1DTO.existingPatientsGrowth, location.apiResponse[0].dp1DTO.newPatients, location.apiResponse[0].dp1DTO.prevNewPatients, location.apiResponse[0].dp1DTO.newPatientsGrowth, location.apiResponse[0].dp1DTO.revenuePerEyewearPairGross, location.apiResponse[0].dp1DTO.prevRevenuePerEyewearPairGross, location.apiResponse[0].dp1DTO.revenuePerEyewearPairGrossGrowth, location.apiResponse[0].dp1DTO.revenuePerPatientGross, location.apiResponse[0].dp1DTO.prevRevenuePerPatientGross, location.apiResponse[0].dp1DTO.revenuePerPatientGrossGrowth, location.apiResponse[0].dp1DTO.multiPairPct, location.apiResponse[0].dp1DTO.prevMultiPairPct, location.apiResponse[0].dp1DTO.multiPairPctGrowth, location.apiResponse[0].dp1DTO.payment, location.apiResponse[0].dp1DTO.prevPayment, location.apiResponse[0].dp1DTO.paymentGrowth, location.apiResponse[0].dp1DTO.patientPayments, location.apiResponse[0].dp1DTO.prevPatientPayments, location.apiResponse[0].dp1DTO.patientPaymentsGrowth, location.apiResponse[0].dp1DTO.revenuePerCLFittingGross, location.apiResponse[0].dp1DTO.prevRevenuePerCLFittingGross, location.apiResponse[0].dp1DTO.revenuePerCLFittingGrossGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVBoA(List<ivankaLocationDTO> locations, bool extraLocations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string fileType = "Export_";
            if(extraLocations)
            {
                fileType = "ExportExtra_";
            }
            string filename = @"\Boa_" + fileType + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6}", "Practice Id", "Practice Name", "Location Id", "Location Name",  "State","Region", "Gross Production");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",\"{4}\",\"{5}\",{6}",location.practiceId, location.practiceName, location.locationId, location.locationName, location.state, GetRegion(location.state) ,location.apiResponse[0].dp1DTO.grossProduction);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVAbby(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string fileType = "Export_";
           
            string filename = @"\Abb_" + fileType + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER
            //grossProduction,distinctpatientid,refractions,contactLensFittings,contactLensCaptureRate,
            //opticalCaptureRate,clUnits,contactLensDollars
            
           // var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50}\r\n",  "Location Id", "Location Name", "Practice Name", "State", "Address", "Address2", "City", "State", "Zip","Country", "Phone", "AbbCustomerId","AbbCustomerIdWithShipTo", "Website Url","Account Type" ,"Alcon Program Flag","Gross Production", "Distinct Patient Count", "Refractions", "Contact Lens Fittings", "Previous Contact Lens Fittings", "Contact Lens Capture Rate", "Optical Capture Rate", "SCL Units", "Previous Contact Lens Units", "Contact Lens Dollars", "Frames Dollars","Frame Units","Spectacle Lens Sales","Spectacle Lens Units","Specialty Lens","CL Toric","CL Multifocal","CL Specialty","CL Spherical","CL Daily","CL Oneweek","CL Twoweek","CL Monthly","Single Vision","Bifocal","Trifocal","Progressive","Premium Progressive","Premium Single","Anti-Reflective","Photochromatic","Polarization", "CL Material Revenue", "Previous CL Material Revenue", "Lab Units");
            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49}\r\n", "Location Id", "Location Name", "Practice Name", "State", "Address", "Address2", "City", "State", "Zip", "Country", "Phone", "AbbCustomerId", "AbbCustomerIdWithShipTo", "Website Url", "Account Type", "Alcon Program Flag", "Gross Production", "Distinct Patient Count", "Refractions", "Contact Lens Fittings",  "Contact Lens Capture Rate", "Optical Capture Rate",  "Contact Lens Dollars", "Frames Dollars", "Frame Units", "Spectacle Lens Sales", "Spectacle Lens Units", "Specialty Lens", "CL Toric", "CL Multifocal", "CL Specialty", "CL Spherical", "CL Daily", "CL Oneweek", "CL Twoweek", "CL Monthly", "Single Vision", "Bifocal", "Trifocal", "Progressive", "Premium Progressive", "Premium Single", "Anti-Reflective", "Photochromatic", "Polarization", "CL Material Revenue", "SCL Units", "Lab Units", "Alcon Marketshare", "Date Range");

            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0 && location.apiResponse[0].dp1DTO.grossProduction != "0")
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},\"{49}\"",  location.locationId, location.locationName, location.practiceName,location.state,location.address, location.address2, location.city,location.state,location.zip,location.country,location.officePhone,location.abbNumber, location.abbShipTo, location.url,location.accountType, location.program,location.apiResponse[0].dp1DTO.grossProduction, location.apiResponse[0].dp1DTO.distinctpatientid, location.apiResponse[0].dp1DTO.refractions, location.apiResponse[0].dp1DTO.contactLensFittings,  location.apiResponse[0].dp1DTO.contactLensCaptureRate, location.apiResponse[0].dp1DTO.opticalCaptureRate, location.apiResponse[0].dp1DTO.contactLensDollarsGross, location.apiResponse[0].dp1DTO.framesDollarsGross, location.apiResponse[0].dp1DTO.frameUnits, location.apiResponse[0].dp1DTO.spectacleLensSalesGross, location.apiResponse[0].dp1DTO.spectacleLensUnits, location.apiResponse[0].dp1DTO.specialty, location.apiResponse[0].dp1DTO.cltoric, location.apiResponse[0].dp1DTO.clmultifocal, location.apiResponse[0].dp1DTO.clspecialty, location.apiResponse[0].dp1DTO.clspherical, location.apiResponse[0].dp1DTO.cldaily, location.apiResponse[0].dp1DTO.cloneweek, location.apiResponse[0].dp1DTO.cltwoweek, location.apiResponse[0].dp1DTO.clmonthly, location.apiResponse[0].dp1DTO.singleVision, location.apiResponse[0].dp1DTO.bifocal, location.apiResponse[0].dp1DTO.trifocal, location.apiResponse[0].dp1DTO.progressive, location.apiResponse[0].dp1DTO.premiumProgressive, location.apiResponse[0].dp1DTO.premiumSingle, location.apiResponse[0].dp1DTO.antireflective, location.apiResponse[0].dp1DTO.photochromatic, location.apiResponse[0].dp1DTO.polarization, location.apiResponse[0].dp1DTO.clmaterialscurrencyGross, location.apiResponse[0].dp1DTO.clUnits, location.apiResponse[0].dp1DTO.lensCount, location.apiResponse[0].dp1DTO.alconClmarketshare, locations[0].startDate.ToString("yyyy-MM-dd") + " TO " + locations[0].endDate.ToString("yyyy-MM-dd"));
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        
        private static void ExportToCSVTotalVision(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string fileType = "VerifyExport_";

            string filename = @"\Analyze_" + fileType + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);
            //L,O,P,Q,U(EHR, Alcon PRogram, Alcon Marketshare,Account type, Website)
            var csv = new StringBuilder();
            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}\r\n", "Location Id", "Location Name", "Practice Name", "State", "Address", "Address2", "City", "State", "Zip", "Country", "Phone", "AbbCustomerId", "AbbCustomerIdWithShipTo", "Gross Production", "Distinct Patient Count", "SCL Units", "CL Gross Sales", "Start Date", "End Date", "Group");

            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0 && location.apiResponse[0].dp1DTO.grossProduction != "0")
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",{13},{14},{15},{16},{17},{18},\"{19}\"", location.locationId, location.locationName, location.practiceName, location.state, location.address, location.address2, location.city, location.state, location.zip, location.country, location.officePhone, location.abbNumber, location.abbShipTo, location.apiResponse[0].dp1DTO.grossProduction, location.apiResponse[0].dp1DTO.distinctpatientid, location.apiResponse[0].dp1DTO.clUnits, location.apiResponse[0].dp1DTO.contactLensDollarsGrossUnlocked, locations[0].startDate.ToString("yyyy-MM-dd"), locations[0].endDate.ToString("yyyy-MM-dd"), location.group);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVVerify(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string fileType = "VerifyExport_";

            string filename = @"\Analyze_" + fileType + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31}\r\n", "Location Id", "Location Name", "Practice Name", "State", "Address", "Address2", "City", "State", "Zip", "Country", "Phone", "EHR", "AbbCustomerId", "AbbCustomerIdWithShipTo", "Website Url", "Account Type", "Alcon Program Flag", "Gross Production", "Distinct Patient Count", "SCL Units", "Alcon Marketshare", "CL Gross Sales", "paymentsByPatient(units)", "paymentsByInsurance(units)", "paymentsByInsuranceVSP(units)", "paymentsByInsuranceEyemed(units)", "existingPatients", "newPatients", "refractions", "Start Date", "End Date", "Group");

            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0 && location.apiResponse[0].dp1DTO.grossProduction != "0")
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},\"{31}\"", location.locationId, location.locationName, location.practiceName, location.state, location.address, location.address2, location.city, location.state, location.zip, location.country, location.officePhone,location.emrName, location.abbNumber, location.abbShipTo, location.url, location.accountType, location.program, location.apiResponse[0].dp1DTO.grossProduction, location.apiResponse[0].dp1DTO.distinctpatientid, location.apiResponse[0].dp1DTO.clUnits, location.apiResponse[0].dp1DTO.alconClmarketshare,  location.apiResponse[0].dp1DTO.contactLensDollarsGrossUnlocked, location.apiResponse[0].dp1DTO.paymentsByPatient, location.apiResponse[0].dp1DTO.paymentsByInsurance, location.apiResponse[0].dp1DTO.paymentsByInsuranceVSP, location.apiResponse[0].dp1DTO.paymentsByInsuranceEyemed, location.apiResponse[0].dp1DTO.existingPatients, location.apiResponse[0].dp1DTO.newPatients, location.apiResponse[0].dp1DTO.refractions,locations[0].startDate.ToString("yyyy-MM-dd"), locations[0].endDate.ToString("yyyy-MM-dd"), location.group);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCooperCLMarketshare(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Cooper_CLMarketshareExport_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "Practice Id", "Practice Name", "Location Id", "Location Name", "State",  "Clmarketshare", "Previous Clmarketshare", "Clmarketshare Growth");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",\"{4}\",{5},{6},{7}", location.practiceId, location.practiceName, location.locationId, location.locationName, location.state, location.apiResponse[0].dp1DTO.clmarketshare, location.apiResponse[0].dp1DTO.prevClmarketshare, location.apiResponse[0].dp1DTO.clmarketshareGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVAlconXceleration(List<ivankaLocationDTO> locations,bool practiceLevel)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Alcon_XCelPartnerPlusExport2_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
           

            filePath += filename;
            filePath = filePath.Substring(6);


            var csv = new StringBuilder();
            //HEADER
            //BILLTO AND SHIPTO NEED TO BE ADJUSTED BY ADDDING LOGIC TO PRINT A CERTAIN VALUE DEPENDING ON PRACTICE/LOCATION LEVEL
            //var newLineH = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}", "SFID-ShipTo", "Unified_Customer_Number", "Unified_Customer_Name", "SFID-Billto", "Default_Payer_Number","Default_Payer_Name", "Analyze PID Id", "Analyze LOC Id", "Analyze Practice/Location Name", "Time Frame - Baseline", "Time Frame - Marketshare Period to Date", "Marketshare Baseline - Practice Level", "Marketshare Baseline - Location Level", "Marketshare Period to Date - Practice Level", "Marketshare Period to Date - Location Level", "Active", "Modified");
            var newLineH = string.Format("{0},{1},\"{2}\",{3},{4},\"{5}\",{6},{7},\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", "SFID-ShipTo", "Unified_Customer_Number", "Unified_Customer_Name", "SFID-Billto", "Default_Payer_Number", "Default_Payer_Name", "Analyze PID Id", "Analyze LOC Id", "Analyze Practice/Location Name", "Time Frame - Baseline", "Time Frame - Marketshare Period to Date", "Marketshare Baseline - Practice Level", "Marketshare Baseline - Location Level", "Marketshare Period to Date - Practice Level", "Marketshare Period to Date - Location Level", "Active", "Modified","Max Billing Date");

            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            //int printHardcodedline = 0;
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');

                    // var newLineT = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", location.SFIDShipTo, location.UCN, location.unified_customer_name, location.SFIDBillTo, location.default_payer_number, location.default_payer_name, location.practiceId, location.locationId, location.locationName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaseline, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.apiResponse[0].dp1DTO.clmarketshare, location.active, 0, location.maxBillingDate.ToString("MM-dd-yyyy"));
                    string maxBillDate = location.maxBillingDate.ToString("MM/dd/yyyy");
                    if (location.maxBillingDate == DateTime.MinValue)
                    {
                        maxBillDate = "N/A";
                        
                    }
                   

                    //if (location.practiceId == 7173 && printHardcodedline == 0)
                    //{
                    //    printHardcodedline = 1;
                    //    maxBillDate = location.endDate.ToString("MM/dd/yyyy");
                    //    //0100142007|0100142007|\"ALAN L BYRD & ASSOC\"|0100142007|0100142007|\"Alan L Byrd & Assoc OD PA\"|7173|7173|\"Dr Alan L Byrd and Associates  OD PA\"|01/01/2021 - 12/31/2021|01/01/2022 - 06/26/2022|67.48|67.48|72.16|72.16|1|0|6/24/2022
                    //    var newLinePracticeLevel = string.Format("{0},{1},\"{2}\",{3},{4},\"{5}\",{6},{7},\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", "0100142007", "0100142007", "ALAN L BYRD & ASSOC", "0100142007", "0100142007", "Alan L Byrd & Assoc OD PA", "7173", "7173", location.practiceName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.active, 0, maxBillDate);

                    //    csv.AppendLine(newLinePracticeLevel);
                    //}
                    var newLineT = string.Format("{0},{1},\"{2}\",{3},{4},\"{5}\",{6},{7},\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", location.SFIDShipTo, location.UCN, location.unified_customer_name, location.SFIDBillTo, location.default_payer_number, location.default_payer_name, location.practiceId, location.locationId, location.locationName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaseline, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.apiResponse[0].dp1DTO.clmarketshare, location.active, 0, maxBillDate);
                   
                    if(location.appendDPNToPID)
                        newLineT = string.Format("{0},{1},\"{2}\",{3},{4},\"{5}\",{6},{7},\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", location.SFIDShipTo, location.UCN, location.unified_customer_name, location.SFIDBillTo, location.default_payer_number, location.default_payer_name, location.practiceId.ToString() + "-" + location.default_payer_number, location.locationId, location.locationName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaseline, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.apiResponse[0].dp1DTO.clmarketshare, location.active, 0, maxBillDate);

                    //var newLineT = string.Format("\"{0}\"|\"{1}\"|\"{2}\"|\"{3}\"|\"{4}\"|\"{5}\"|\"{6}\"|\"{7}\"|\"{8}\"|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}", location.SFIDShipTo, location.UCN,location.unified_customer_name, location.SFIDBillTo, location.default_payer_number,location.default_payer_name, location.practiceId, location.locationId, location.locationName, location.timeframeBaseline,location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaseline, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel,location.apiResponse[0].dp1DTO.clmarketshare, location.active,0);
                    csv.AppendLine(newLineT);
                    if (location.printPracticeLevel)
                    {
                        //var newLinePracticeLevel = string.Format("\"{0}\"|\"{1}\"|\"{2}\"|\"{3}\"|\"{4}\"|\"{5}\"|\"{6}\"|\"{7}\"|\"{8}\"|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}", location.default_payer_number, location.default_payer_number, location.unified_customer_name, location.default_payer_number, location.default_payer_number, location.default_payer_name, location.practiceId, location.practiceId, location.practiceName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, " ", location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, " ", location.active, 0);
                        var newLinePracticeLevel = string.Format("{0},{1},\"{2}\",{3},{4},\"{5}\",{6},{7},\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", location.default_payer_number, location.default_payer_number, location.default_payer_name, location.default_payer_number, location.default_payer_number, location.default_payer_name, location.practiceId, location.practiceId, location.practiceName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, 0, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, 0, location.active, 0, maxBillDate);

                        if (location.appendDPNToPID)
                            newLinePracticeLevel = string.Format("{0},{1},\"{2}\",{3},{4},\"{5}\",{6},{7},\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", location.default_payer_number, location.default_payer_number, location.default_payer_name, location.default_payer_number, location.default_payer_number, location.default_payer_name, location.practiceId + "-" + location.default_payer_number, location.practiceId + "-" + location.default_payer_number, location.practiceName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, 0, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, 0, location.active, 0, maxBillDate);
                        
                           
                        csv.AppendLine(newLinePracticeLevel);
                    }



                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVAlconXceleration2(List<ivankaLocationDTO> locations, bool practiceLevel)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Alcon_XCelPartnerPlusExport_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";


            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER
            //BILLTO AND SHIPTO NEED TO BE ADJUSTED BY ADDDING LOGIC TO PRINT A CERTAIN VALUE DEPENDING ON PRACTICE/LOCATION LEVEL
            
            var newLineH = string.Format("{0}|{1}|”{2}”|{3}|{4}|”{5}”|{6}|{7}|”{8}”|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}", "SFID-ShipTo", "Unified_Customer_Number", "Unified_Customer_Name", "SFID-Billto", "Default_Payer_Number","Default_Payer_Name", "Analyze PID Id", "Analyze LOC Id", "Analyze Practice/Location Name", "Time Frame - Baseline", "Time Frame - Marketshare Period to Date", "Marketshare Baseline - Practice Level", "Marketshare Baseline - Location Level", "Marketshare Period to Date - Practice Level", "Marketshare Period to Date - Location Level", "Active", "Modified", "Max Billing Date");
            //var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17}", "SFID-ShipTo", "Unified_Customer_Number", "Unified_Customer_Name", "SFID-Billto", "Default_Payer_Number", "Default_Payer_Name", "Analyze PID Id", "Analyze LOC Id", "Analyze Practice/Location Name", "Time Frame - Baseline", "Time Frame - Marketshare Period to Date", "Marketshare Baseline - Practice Level", "Marketshare Baseline - Location Level", "Marketshare Period to Date - Practice Level", "Marketshare Period to Date - Location Level", "Active", "Modified", "Max Billing Date");

            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            //int printHardcodedline = 0;
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');

                    string maxBillDate = location.maxBillingDate.ToString("MM/dd/yyyy");
                    if (location.maxBillingDate == DateTime.MinValue)
                    {
                        maxBillDate = "N/A";

                    }
                   

                    //if (location.practiceId == 7173 && printHardcodedline == 0)
                    //{
                    //    printHardcodedline = 1;
                    //    maxBillDate = location.endDate.ToString("MM/dd/yyyy");
                    //    //0100142007|0100142007|”ALAN L BYRD & ASSOC”|0100142007|0100142007|”Alan L Byrd & Assoc OD PA”|7173|7173|”Dr Alan L Byrd and Associates  OD PA”|01/01/2021 - 12/31/2021|01/01/2022 - 06/26/2022|67.48|67.48|72.16|72.16|1|0|6/24/2022
                    //    var newLinePracticeLevel = string.Format("{0}|{1}|”{2}”|{3}|{4}|”{5}”|{6}|{7}|”{8}”|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}", "0100142007", "0100142007", "ALAN L BYRD & ASSOC", "0100142007", "0100142007", "Alan L Byrd & Assoc OD PA", "7173", "7173", "Dr Alan L Byrd and Associates  OD PA", location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.active, 0, maxBillDate);


                    //    csv.AppendLine(newLinePracticeLevel);
                    //}
                    var newLineT = string.Format("{0}|{1}|”{2}”|{3}|{4}|”{5}”|{6}|{7}|”{8}”|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}", location.SFIDShipTo, location.UCN, location.unified_customer_name, location.SFIDBillTo, location.default_payer_number, location.default_payer_name, location.practiceId, location.locationId, location.locationName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaseline, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.apiResponse[0].dp1DTO.clmarketshare, location.active, 0, maxBillDate);
                    if (location.appendDPNToPID)
                         newLineT = string.Format("{0}|{1}|”{2}”|{3}|{4}|”{5}”|{6}|{7}|”{8}”|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}", location.SFIDShipTo, location.UCN, location.unified_customer_name, location.SFIDBillTo, location.default_payer_number, location.default_payer_name, location.practiceId.ToString() + "-" + location.default_payer_number, location.locationId, location.locationName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaseline, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.apiResponse[0].dp1DTO.clmarketshare, location.active, 0, maxBillDate);
                        // newLineT = string.Format("{0},{1},\"{2}\",{3},{4},\"{5}\",{6},{7},\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", location.SFIDShipTo, location.UCN, location.unified_customer_name, location.SFIDBillTo, location.default_payer_number, location.default_payer_name, location.practiceId.ToString() + "-" + location.default_payer_number, location.locationId, location.locationName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, location.apiResponse[0].dp1DTO.clmarketsharebaseline, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, location.apiResponse[0].dp1DTO.clmarketshare, location.active, 0, maxBillDate);

                    csv.AppendLine(newLineT);
                    if (location.printPracticeLevel)
                    {
                        var newLinePracticeLevel = string.Format("{0}|{1}|”{2}”|{3}|{4}|”{5}”|{6}|{7}|”{8}”|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}", location.default_payer_number, location.default_payer_number, location.unified_customer_name, location.default_payer_number, location.default_payer_number, location.default_payer_name, location.practiceId, location.practiceId, location.practiceName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, 0, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, 0, location.active, 0, maxBillDate);
                        //var newLinePracticeLevel = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",{9},{10},{11},{12},{13},{14},{15},{16},{17}", location.default_payer_number, location.default_payer_number, location.default_payer_name, location.default_payer_number, location.default_payer_number, location.default_payer_name, location.practiceId, location.practiceId, location.practiceName, location.timeframeBaseline, location.timeframeMarketshare, location.apiResponse[0].dp1DTO.clmarketsharebaselinePracticeLevel, 0, location.apiResponse[0].dp1DTO.clmarketsharePracticeLevel, 0, location.active, 0, "");

                        csv.AppendLine(newLinePracticeLevel);
                    }

                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVAlconX(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Alcon_CLMarketshareExport_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{31},{32}", "Practice Id", "Practice Name", "Location Id", "Location Name", "State", "Clmarketshare", "Previous Clmarketshare", "Clmarketshare Growth", "baseline", "ClmarketshareAFTERCHANGES", "Previous ClmarketshareAFTERCHANGES", "Clmarketshare GrowthAFTERCHANGES", "cldaily", "prevCldaily", "cldailyGrowth", "cltoric", "prevCltoric", "cltoricGrowth", "clmultifocal", "prevClmultifocal", "clmultifocalGrowth", "clspherical", "prevClspherical", "clsphericalGrowth", "cltwoweek", "prevCltwoweek", "cltwoweekGrowth", "clmonthly", "prevClmonthly", "clmonthlyGrowth", "cloneweek", "prevCloneweek", "cloneweekGrowth");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",\"{4}\",{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{31},{32}", location.practiceId, location.practiceName, location.locationId, location.locationName, location.state, location.apiResponse[0].dp1DTO.clmarketshare, location.apiResponse[0].dp1DTO.prevClmarketshare, location.apiResponse[0].dp1DTO.clmarketshareGrowth, location.apiResponse[0].dp1DTO.clmarketsharebaseline,location.apiResponse[0].dp1DTO.clmarketshareTest, location.apiResponse[0].dp1DTO.prevClmarketshareTest, location.apiResponse[0].dp1DTO.clmarketshareTestGrowth, location.apiResponse[0].dp1DTO.cldaily, location.apiResponse[0].dp1DTO.prevCldaily, location.apiResponse[0].dp1DTO.cldailyGrowth, location.apiResponse[0].dp1DTO.cltoric, location.apiResponse[0].dp1DTO.prevCltoric, location.apiResponse[0].dp1DTO.cltoricGrowth, location.apiResponse[0].dp1DTO.clmultifocal, location.apiResponse[0].dp1DTO.prevClmultifocal, location.apiResponse[0].dp1DTO.clmultifocalGrowth, location.apiResponse[0].dp1DTO.clspherical, location.apiResponse[0].dp1DTO.prevClspherical, location.apiResponse[0].dp1DTO.clsphericalGrowth, location.apiResponse[0].dp1DTO.cltwoweek, location.apiResponse[0].dp1DTO.prevCltwoweek, location.apiResponse[0].dp1DTO.cltwoweekGrowth, location.apiResponse[0].dp1DTO.clmonthly, location.apiResponse[0].dp1DTO.prevClmonthly, location.apiResponse[0].dp1DTO.clmonthlyGrowth, location.apiResponse[0].dp1DTO.cloneweek, location.apiResponse[0].dp1DTO.prevCloneweek, location.apiResponse[0].dp1DTO.cloneweekGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCooperCLMarketshare2(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Cooper_CLMarketshareExport_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", "Practice Id", "Practice Name", "Location Id", "Location Name", "State", "Region", "Gross Production", "Previous Gross Production", "Gross Production Growth", "Clmarketshare", "Previous Clmarketshare", "Clmarketshare Growth");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",\"{4}\",\"{5}\",{6},{7},{8},{9},{10},{11}", location.practiceId, location.practiceName, location.locationId, location.locationName, location.state, GetRegion(location.state), location.apiResponse[0].dp1DTO.grossProduction, location.apiResponse[0].dp1DTO.prevGrossProduction, location.apiResponse[0].dp1DTO.grossProductionGrowth, location.apiResponse[0].dp1DTO.clmarketshare, location.apiResponse[0].dp1DTO.prevClmarketshare, location.apiResponse[0].dp1DTO.clmarketshareGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVDTP(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\DTP_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23}", "Practice Id", "Practice Name", "Location Id", "Location Name", "State", "Region", "Gross Production", "Previous Gross Production", "Gross Production Growth", "Contact Lens dollars", "Previous Contact Lens dollars", "Contact Lens dollars Growth", "CL Capture Rate", "Previous CL Capture Rate", "CL Capture Rate Growth", "CL Annual Supply Pct", "Previous CL Annual Supply Pct", "CL Annual Supply Pct Growth", "Revenue Per CL Fitting", "Previous Revenue Per CL Fitting", "Revenue Per CL Fitting Growth", "CL Daily", "Previous CL Daily", "CL Daily Growth");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},\"{1}\",{2},\"{3}\",\"{4}\",\"{5}\",{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23}", location.practiceId, location.practiceName, location.locationId, location.locationName, location.state, GetRegion(location.state), location.apiResponse[0].dp1DTO.grossProduction, location.apiResponse[0].dp1DTO.prevGrossProduction, location.apiResponse[0].dp1DTO.grossProductionGrowth, location.apiResponse[0].dp1DTO.contactLensDollarsGross,location.apiResponse[0].dp1DTO.prevContactLensDollarsGross, location.apiResponse[0].dp1DTO.contactLensDollarsGrossGrowth, location.apiResponse[0].dp1DTO.contactLensCaptureRate, location.apiResponse[0].dp1DTO.prevContactLensCaptureRate, location.apiResponse[0].dp1DTO.contactLensCaptureRateGrowth, location.apiResponse[0].dp1DTO.clAnnualSupplyPct, location.apiResponse[0].dp1DTO.prevClAnnualSupplyPct, location.apiResponse[0].dp1DTO.ClAnnualSupplyPctGrowth, location.apiResponse[0].dp1DTO.revenuePerCLFittingGross, location.apiResponse[0].dp1DTO.prevRevenuePerCLFittingGross, location.apiResponse[0].dp1DTO.revenuePerCLFittingGrossGrowth, location.apiResponse[0].dp1DTO.clDailyGross, location.apiResponse[0].dp1DTO.prevClDailyGross, location.apiResponse[0].dp1DTO.clDailyGrossGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSVTaye(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\NonTaye_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", "Practice Id", "Location Id","Practice Name", "Location Name", "Gross Revenue", "Eye Exams Revenue", "Contact Lens Revenue", "Lens & Frames Revenue", "Unique Patients", "Eye Exams", "Patient Share Using Insurance", "Annual Visits Per Patient");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},{1},\"{2}\",\"{3}\",{4},{5},{6},{7},{8},{9},{10},{11}", location.practiceId,location.locationId,location.practiceName, location.locationName, location.apiResponse[0].tayeDTO.grossProduction.Trim(), location.apiResponse[0].tayeDTO.examRevenueGross.Trim(), location.apiResponse[0].tayeDTO.clmaterialscurrencyGross.Trim(), location.apiResponse[0].tayeDTO.lensAndFramesRevenue.Trim(), location.apiResponse[0].tayeDTO.distinctpatientid.Trim(), location.apiResponse[0].tayeDTO.refractions.Trim(), location.apiResponse[0].tayeDTO.patientsUsingInsurance.Trim(), location.apiResponse[0].tayeDTO.avgPatientVisits.Trim());
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        
        private static void ExportToCSVVSDP2(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\VS1_Exportv4_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32}", "Practice Id", "Location Id", "Practice Name", "Location Name","State", "grossProduction", "revenuePerCLFittingGross", "revenuePerPatientGross", "clfittingscurrencyGross", "clmultifocalGross", "contactLensCaptureRate", "contactLensFittings", "clmultifocal", "contactLensDollarsGross", "cltoricGross","refractions","refractionsGross"    , "opticalDollarsGross", "officeVisitsDollarsGross", "ancillaryGross", "medicalDollarsGross", "totalCaptureRate", "paymentsPerPatient", "paymentsPerRefraction", "payment", "revenuePerRefractionGross", "production", "framesDollarsGross", "frameCaptureRate", "revenuePerEyewearPairGross", "spectacleLensSalesGross", "opticalCaptureRate", "revenuePerMedicalPatient");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},{1},\"{2}\",\"{3}\",\"{4}\",{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32}", location.practiceId, location.locationId, location.practiceName, location.locationName, location.state, location.apiResponse[0].dp1DTO.grossProduction.Trim(), location.apiResponse[0].dp1DTO.revenuePerCLFittingGross.Trim(), location.apiResponse[0].dp1DTO.revenuePerPatientGross.Trim(), location.apiResponse[0].dp1DTO.clfittingscurrencyGross.Trim(), location.apiResponse[0].dp1DTO.clmultifocalGross.Trim(), location.apiResponse[0].dp1DTO.contactLensCaptureRate.Trim(), location.apiResponse[0].dp1DTO.contactLensFittings.Trim(), location.apiResponse[0].dp1DTO.clmultifocal.Trim(), location.apiResponse[0].dp1DTO.contactLensDollarsGross.Trim(), location.apiResponse[0].dp1DTO.cltoricGross.Trim(), location.apiResponse[0].dp1DTO.refractions.Trim(), location.apiResponse[0].dp1DTO.refractionsGross.Trim(), location.apiResponse[0].dp1DTO.opticalDollarsGross.Trim(), location.apiResponse[0].dp1DTO.officeVisitsDollarsGross.Trim(), location.apiResponse[0].dp1DTO.ancillaryGross.Trim(), location.apiResponse[0].dp1DTO.medicalDollarsGross.Trim(), location.apiResponse[0].dp1DTO.totalCaptureRate.Trim(), location.apiResponse[0].dp1DTO.paymentsPerPatient.Trim(), location.apiResponse[0].dp1DTO.paymentsPerRefraction.Trim(), location.apiResponse[0].dp1DTO.payment.Trim(), location.apiResponse[0].dp1DTO.revenuePerRefractionGross.Trim(), location.apiResponse[0].dp1DTO.production.Trim(), location.apiResponse[0].dp1DTO.framesDollarsGross.Trim(), location.apiResponse[0].dp1DTO.frameCaptureRate.Trim(), location.apiResponse[0].dp1DTO.revenuePerEyewearPairGross.Trim(), location.apiResponse[0].dp1DTO.spectacleLensSalesGross.Trim(), location.apiResponse[0].dp1DTO.opticalCaptureRate.Trim(), location.apiResponse[0].dp1DTO.revenuePerMedicalPatient.Trim());
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();
        }
        private static void ExportToCSVVisioneering(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\ViTech_v1_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}, {15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26}", "Practice Id", "Location Id", "Practice Name", "Location Name", "State", "Region", "grossProduction", "previous_grossProduction", "growth_grossProduction", "refractions", "previous_refractions", "growth_refractions", "refractionsGross", "previous_refractionsGross", "growth_refractionsGross",  "contactLensDollarsGross", "previous_contactLensDollarsGross", "growth_contactLensDollarsGross", "clCount", "previous_clCount", "growth_clCount", "clCaptureRate", "previous_clCaptureRate", "growth_clCaptureRate", "openLocationPct", "previous_openLocationPct", "growth_openLocationPct");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},{1},\"{2}\",\"{3}\",\"{4}\",{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}, {15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26}", location.practiceId, location.locationId, location.practiceName, location.locationName, location.state, location.region, location.apiResponse[0].dp1DTO.grossProduction.Trim(), location.apiResponse[0].dp1DTO.prevGrossProduction.Trim(), location.apiResponse[0].dp1DTO.grossProductionGrowth, location.apiResponse[0].dp1DTO.refractions.Trim(), location.apiResponse[0].dp1DTO.prevRefractions.Trim(), location.apiResponse[0].dp1DTO.refractionsGrowth, location.apiResponse[0].dp1DTO.refractionsGross.Trim(), location.apiResponse[0].dp1DTO.prevRefractionsGross.Trim(), location.apiResponse[0].dp1DTO.refractionsGrossGrowth, location.apiResponse[0].dp1DTO.contactLensDollarsGross.Trim(), location.apiResponse[0].dp1DTO.prevContactLensDollarsGross.Trim(), location.apiResponse[0].dp1DTO.contactLensDollarsGrossGrowth, location.apiResponse[0].dp1DTO.clCount.Trim(), location.apiResponse[0].dp1DTO.prevClCount.Trim(), location.apiResponse[0].dp1DTO.clCountGrowth,location.apiResponse[0].dp1DTO.contactLensCaptureRate, location.apiResponse[0].dp1DTO.prevContactLensCaptureRate, location.apiResponse[0].dp1DTO.contactLensCaptureRateGrowth, location.apiResponse[0].dp1DTO.openLocationPct, location.apiResponse[0].dp1DTO.previousOpenLocationPct, location.apiResponse[0].dp1DTO.openLocationPctGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();
        }
        
        private static void ExportToCSVClCr(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\CLCR_v1_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", "Practice Id", "Location Id", "Practice Name", "Location Name", "State", "Region", "grossProduction", "previous_grossProduction", "growth_grossProduction", "contactLensCaptureRate", "previous_contactLensCaptureRate", "growth_contactLensCaptureRate");//
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},{1},\"{2}\",\"{3}\",\"{4}\",{5},{6},{7},{8},{9},{10},{11}", location.practiceId, location.locationId, location.practiceName, location.locationName, location.state, location.region, location.apiResponse[0].dp1DTO.grossProduction.Trim(), location.apiResponse[0].dp1DTO.prevGrossProduction.Trim(), location.apiResponse[0].dp1DTO.grossProductionGrowth, location.apiResponse[0].dp1DTO.contactLensCaptureRate,location.apiResponse[0].dp1DTO.prevContactLensCaptureRate, location.apiResponse[0].dp1DTO.contactLensCaptureRateGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();
        }

        private static void ExportToCSVJobson(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Jobson_v1_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}, {15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54}", "Practice Id", "Location Id", "Practice Name", "Location Name", "State","Region", "grossProduction", "previous_grossProduction", "growth_grossProduction", "refractions", "previous_refractions", "growth_refractions", "refractionsGross", "previous_refractionsGross", "growth_refractionsGross", "frameUnits", "previous_frameUnits", "growth_frameUnits", "framesDollarsGross", "previous_framesDollarsGross", "growth_framesDollarsGross", "contactLensDollarsGross", "previous_contactLensDollarsGross", "growth_contactLensDollarsGross", "clCount", "previous_clCount", "growth_clCount", "spectacleLensUnits", "previous_spectacleLensUnits", "growth_spectacleLensUnits", "lensCount", "previous_lensCount", "growth_lensCount", "spectacleLensSalesGross", "previous_spectacleLensSalesGross", "growth_spectacleLensSalesGross" ,"maxBillingDate", "clAnnualSupply", "previous_clAnnualSupply", "growth_clAnnualSupply", "contactLensCaptureRate", "previous_contactLensCaptureRate", "growth_contactLensCaptureRate", "revenuePerCLFittingGross", "previous_revenuePerCLFittingGross", "growth_revenuePerCLFittingGross", "cldailypct", "previous_cldailypct", "growth_cldailypct", "cltwoweekpct", "previous_cltwoweekpct", "growth_cltwoweekpct", "clmonthlypct", "previous_clmonthlypct", "growth_clmonthlypct");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},{1},\"{2}\",\"{3}\",\"{4}\",{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}, {15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54}", location.practiceId, location.locationId, location.practiceName, location.locationName, location.state,location.region, location.apiResponse[0].dp1DTO.grossProduction.Trim(), location.apiResponse[0].dp1DTO.prevGrossProduction.Trim(), location.apiResponse[0].dp1DTO.grossProductionGrowth, location.apiResponse[0].dp1DTO.refractions.Trim(), location.apiResponse[0].dp1DTO.prevRefractions.Trim(), location.apiResponse[0].dp1DTO.refractionsGrowth, location.apiResponse[0].dp1DTO.refractionsGross.Trim(), location.apiResponse[0].dp1DTO.prevRefractionsGross.Trim(), location.apiResponse[0].dp1DTO.refractionsGrossGrowth, location.apiResponse[0].dp1DTO.frameUnits.Trim(), location.apiResponse[0].dp1DTO.prevFrameUnits.Trim(), location.apiResponse[0].dp1DTO.frameUnitsGrowth, location.apiResponse[0].dp1DTO.framesDollarsGross.Trim(), location.apiResponse[0].dp1DTO.prevFramesDollarsGross.Trim(), location.apiResponse[0].dp1DTO.framesDollarsGrossGrowth, location.apiResponse[0].dp1DTO.contactLensDollarsGross.Trim(), location.apiResponse[0].dp1DTO.prevContactLensDollarsGross.Trim(), location.apiResponse[0].dp1DTO.contactLensDollarsGrossGrowth, location.apiResponse[0].dp1DTO.clCount.Trim(), location.apiResponse[0].dp1DTO.prevClCount.Trim(), location.apiResponse[0].dp1DTO.clCountGrowth, location.apiResponse[0].dp1DTO.spectacleLensUnits.Trim(), location.apiResponse[0].dp1DTO.prevSpectacleLensUnits.Trim(), location.apiResponse[0].dp1DTO.spectacleLensUnitsGrowth, location.apiResponse[0].dp1DTO.lensCount.Trim(), location.apiResponse[0].dp1DTO.prevLensCount.Trim(), location.apiResponse[0].dp1DTO.lensCountGrowth, location.apiResponse[0].dp1DTO.spectacleLensSalesGross.Trim(), location.apiResponse[0].dp1DTO.prevSpectacleLensSalesGross.Trim(), location.apiResponse[0].dp1DTO.spectacleLensSalesGrossGrowth, location.maxBillingDate, location.apiResponse[0].dp1DTO.clAnnualSupply.Trim(), location.apiResponse[0].dp1DTO.prevClAnnualSupply.Trim(), location.apiResponse[0].dp1DTO.ClAnnualSupplyGrowth, location.apiResponse[0].dp1DTO.contactLensCaptureRate.Trim(), location.apiResponse[0].dp1DTO.prevContactLensCaptureRate.Trim(), location.apiResponse[0].dp1DTO.contactLensCaptureRateGrowth, location.apiResponse[0].dp1DTO.revenuePerCLFittingGross.Trim(), location.apiResponse[0].dp1DTO.prevRevenuePerCLFittingGross.Trim(), location.apiResponse[0].dp1DTO.revenuePerCLFittingGrossGrowth, location.apiResponse[0].dp1DTO.cldailypct.Trim(), location.apiResponse[0].dp1DTO.prevCldailypct.Trim(), location.apiResponse[0].dp1DTO.cldailypctGrowth, location.apiResponse[0].dp1DTO.cltwoweekpct.Trim(), location.apiResponse[0].dp1DTO.prevCltwoweekpct.Trim(), location.apiResponse[0].dp1DTO.cltwoweekpctGrowth, location.apiResponse[0].dp1DTO.clmonthlypct.Trim(), location.apiResponse[0].dp1DTO.prevClmonthlypct.Trim(), location.apiResponse[0].dp1DTO.clmonthlypctGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();
        }
        private static void ExportToCSVJobsonTemp(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\Jobson_v1_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}", "Practice Id", "Location Id", "Practice Name", "Location Name", "State", "Region","clAnnualSupply", "previous_clAnnualSupply", "growth_clAnnualSupply");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},{1},\"{2}\",\"{3}\",\"{4}\",{5},{6},{7},{8}", location.practiceId, location.locationId, location.practiceName, location.locationName, location.state, location.region, location.apiResponse[0].dp1DTO.clAnnualSupplyPct.Trim(), location.apiResponse[0].dp1DTO.prevClAnnualSupplyPct.Trim(), location.apiResponse[0].dp1DTO.ClAnnualSupplyGrowth);
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();
        }
        private static void ExportToCSVDP1(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\DP1_Export_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", "Practice Id", "Location Id", "Practice Name", "Location Name", "revenuePerMedicalPatientGross", "revenuePerCLFittingGross", "spectacleLensSalesGross", "grossProduction","grossProductionPrevious", "grossProductionGrowth", "framesDollarsGross", "medicalDollarsGross");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},{1},\"{2}\",\"{3}\",{4},{5},{6},{7},{8},{9},{10},{11}", location.practiceId, location.locationId, location.practiceName, location.locationName, location.apiResponse[0].dp1DTO.revenuePerMedicalPatientGross.Trim(), location.apiResponse[0].dp1DTO.revenuePerCLFittingGross.Trim(), location.apiResponse[0].dp1DTO.spectacleLensSalesGross.Trim(), location.apiResponse[0].dp1DTO.grossProduction.Trim(), location.apiResponse[0].dp1DTO.prevGrossProduction.Trim(), location.apiResponse[0].dp1DTO.grossProductionGrowth.ToString(), location.apiResponse[0].dp1DTO.framesDollarsGross.Trim(), location.apiResponse[0].dp1DTO.medicalDollarsGross.Trim());
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }


        private static void ExportToCSVDPPractices(List<ivankaLocationDTO> locations)
        {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\PracticesExport_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", "Practice Id", "Location Id", "Practice Name", "Location Name","address","address2","city","state","zip","officePhone","emrName", "grossProduction", "payments");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations)
            {
                if (location.apiResponse.Count > 0)
                {
                    location.practiceName = location.practiceName.Replace(',', ' ');
                    location.locationName = location.locationName.Replace(',', ' ');
                    var newLineT = string.Format("{0},{1},\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",{11},{12}", location.practiceId, location.locationId, location.practiceName, location.locationName,location.address,location.address2,location.city,location.state,location.zip,location.officePhone,location.emrName, location.apiResponse[0].dp1DTO.grossProduction.Trim(), location.apiResponse[0].dp1DTO.payment.Trim());
                    csv.AppendLine(newLineT);
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static void ExportToCSV2(List<ivankaLocationDTO> locations) {
            var filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filename = @"\NonABB_CL_" + locations[0].startDate.ToString("yyyy-MM-dd") + "_" + locations[0].endDate.ToString("yyyy-MM-dd") + ".csv";
            filePath += filename;
            filePath = filePath.Substring(6);

            var csv = new StringBuilder();
            //HEADER

            var newLineH = string.Format("{0},\"{1}\",\"{2}\",{3},{4},{5}", "GlimpseId", "practiceName", "locationName", "Soft Contact Lens Gross", "Specialty Contact Lens Gross", "Spectacle Lens Gross");
            csv.AppendLine(newLineH);
            File.WriteAllText(filePath, csv.ToString());
            csv.Clear();
            foreach (var location in locations) {
                try {
                    if (location.apiResponse.Count > 0) {
                        if (location.apiResponse[0].KpiGross.clsoftGross.Trim() == "0" && location.apiResponse[0].KpiGross.clspecialtyGross.Trim() == "0" && location.apiResponse[0].KpiGross.spectacleLensSalesGross.Trim() == "0") {
                            //SKIP
                        } else {
                            var newLineT = string.Format("{0},\"{1}\",\"{2}\",{3},{4},{5}", location.locationId, location.practiceName, location.locationName, location.apiResponse[0].KpiGross.clsoftGross.Trim(), location.apiResponse[0].KpiGross.clspecialtyGross.Trim(), location.apiResponse[0].KpiGross.spectacleLensSalesGross.Trim());
                            csv.AppendLine(newLineT);
                        }
                    }
                }catch(Exception ex) {
                    Console.WriteLine("Error exporting line for location " + location.locationId + " error: " + ex.Message.ToString());
                    Logger.log("Error exporting line for location " + location.locationId + " error: " + ex.Message.ToString());
                }
            }
            File.AppendAllText(filePath, csv.ToString());
            csv.Clear();

        }
        private static APIWebsiteResponseModelDTO MapToExportToCSV(APIWebsiteResponseModelDTO apiResponse) {
            try {
                foreach (var item in apiResponse.metrics) {
                    switch (item.key) {

                        //,specialty,
                        //singleVision,bifocal,trifocal, progressive,premiumProgressive,premiumSingle,antireflective,polarization,clspecialty
                        case "unityLensCount":
                            apiResponse.dp1DTO.unityLensCount = item.total.ToString();
                            apiResponse.dp1DTO.prevUnityLensCount = item.prevTotal.ToString();
                            apiResponse.dp1DTO.unityLensCountGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "paymentsByPatient":
                            apiResponse.dp1DTO.paymentsByPatient = item.total.ToString();
                            apiResponse.dp1DTO.prevPaymentsByPatient = item.prevTotal.ToString();
                            apiResponse.dp1DTO.paymentsByPatientGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "paymentsByInsurance":
                            apiResponse.dp1DTO.paymentsByInsurance = item.total.ToString();
                            apiResponse.dp1DTO.prevPaymentsByInsurance = item.prevTotal.ToString();
                            apiResponse.dp1DTO.paymentsByInsuranceGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "paymentsByInsuranceVSP":
                            apiResponse.dp1DTO.paymentsByInsuranceVSP = item.total.ToString();
                            apiResponse.dp1DTO.prevPaymentsByInsuranceVSP = item.prevTotal.ToString();
                            apiResponse.dp1DTO.paymentsByInsuranceVSPGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "paymentsByInsuranceEyemed":
                            apiResponse.dp1DTO.paymentsByInsuranceEyemed = item.total.ToString();
                            apiResponse.dp1DTO.prevPaymentsByInsuranceEyemed = item.prevTotal.ToString();
                            apiResponse.dp1DTO.paymentsByInsuranceEyemedGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "alconClMarketshare":
                             apiResponse.dp1DTO.alconClmarketshare = item.total.ToString();
                             apiResponse.dp1DTO.prevAlconClmarketshare = item.prevTotal.ToString();
                             apiResponse.dp1DTO.alconClmarketshareGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "clmaterialscurrencyGross": 
                            apiResponse.dp1DTO.clmaterialscurrencyGross = item.total.ToString();
                            apiResponse.dp1DTO.prevClmaterialscurrencyGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clmaterialscurrencyGrossGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "specialty": apiResponse.dp1DTO.specialty = item.total.ToString(); break;
                        case "singleVision": apiResponse.dp1DTO.singleVision = item.total.ToString(); break;
                        case "bifocal": apiResponse.dp1DTO.bifocal = item.total.ToString(); break;
                        case "trifocal": apiResponse.dp1DTO.trifocal = item.total.ToString(); break;
                        case "progressive": apiResponse.dp1DTO.progressive = item.total.ToString(); break;
                        case "premiumProgressive": apiResponse.dp1DTO.premiumProgressive = item.total.ToString(); break;
                        case "premiumSingle": apiResponse.dp1DTO.premiumSingle = item.total.ToString(); break;
                        case "antireflective": apiResponse.dp1DTO.antireflective = item.total.ToString(); break;
                        case "polarization": apiResponse.dp1DTO.polarization = item.total.ToString(); break;
                        case "clspecialty": apiResponse.dp1DTO.clspecialty = item.total.ToString(); break;

                        case "spectacleLensUnits":
                            apiResponse.dp1DTO.spectacleLensUnits = item.total.ToString();
                            apiResponse.dp1DTO.prevSpectacleLensUnits = item.prevTotal.ToString();
                            apiResponse.dp1DTO.spectacleLensUnitsGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "frameUnits":
                            apiResponse.dp1DTO.frameUnits = item.total.ToString();
                            apiResponse.dp1DTO.prevFrameUnits = item.prevTotal.ToString();
                           
                            apiResponse.dp1DTO.frameUnitsGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "clmarketshare":
                            apiResponse.dp1DTO.clmarketshare = item.total.ToString();
                            apiResponse.dp1DTO.prevClmarketshare = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clmarketshareGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "clmarketsharebaselineCached":
                            apiResponse.dp1DTO.clmarketsharebaseline = item.total.ToString();
                            apiResponse.dp1DTO.prevClmarketsharebaseline = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clmarketsharebaselineGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                            
                        case "clmarketshareTest":
                            apiResponse.dp1DTO.clmarketshareTest = item.total.ToString();
                            apiResponse.dp1DTO.prevClmarketshareTest = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clmarketshareTestGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "cldaily":
                            apiResponse.dp1DTO.cldaily = item.total.ToString();
                            apiResponse.dp1DTO.prevCldaily = item.prevTotal.ToString();
                            apiResponse.dp1DTO.cldailyGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "cltoric":
                            apiResponse.dp1DTO.cltoric = item.total.ToString();
                            apiResponse.dp1DTO.prevCltoric = item.prevTotal.ToString();
                            apiResponse.dp1DTO.cltoricGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "clmultifocal":
                            apiResponse.dp1DTO.clmultifocal = item.total.ToString();
                            apiResponse.dp1DTO.prevClmultifocal = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clmultifocalGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "clspherical":
                            apiResponse.dp1DTO.clspherical = item.total.ToString();
                            apiResponse.dp1DTO.prevClspherical = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clsphericalGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "cltwoweek":
                            apiResponse.dp1DTO.cltwoweek = item.total.ToString();
                            apiResponse.dp1DTO.prevCltwoweek = item.prevTotal.ToString();
                            apiResponse.dp1DTO.cltwoweekGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "clmonthly":
                            apiResponse.dp1DTO.clmonthly = item.total.ToString();
                            apiResponse.dp1DTO.prevClmonthly = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clmonthlyGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "cloneweek":
                            apiResponse.dp1DTO.cloneweek = item.total.ToString();
                            apiResponse.dp1DTO.prevCloneweek = item.prevTotal.ToString();
                            apiResponse.dp1DTO.cloneweekGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                      
                        case "cltoricGross": apiResponse.dp1DTO.cltoricGross = item.total.ToString(); break;
                        case "revenuePerMedicalPatientGross": apiResponse.dp1DTO.revenuePerMedicalPatientGross = item.total.ToString(); break;
                        case "distinctpatientid":
                            apiResponse.dp1DTO.distinctpatientid = item.total.ToString();
                            apiResponse.dp1DTO.prevDistinctpatientid = item.prevTotal.ToString();
                            apiResponse.dp1DTO.distinctpatientidGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "revenuePerCLFittingGross": 
                            apiResponse.dp1DTO.revenuePerCLFittingGross = item.total.ToString();
                            apiResponse.dp1DTO.prevRevenuePerCLFittingGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.revenuePerCLFittingGrossGrowth = CalculateGrowth(item.total, item.prevTotal);
                      
                            break;
                        case "spectacleLensSalesGross": apiResponse.dp1DTO.spectacleLensSalesGross = item.total.ToString(); break;
                        case "framesDollarsGross": apiResponse.dp1DTO.framesDollarsGross = item.total.ToString(); break;
                        case "grossProduction":
                            apiResponse.dp1DTO.grossProduction = item.total.ToString();
                            apiResponse.dp1DTO.prevGrossProduction = item.prevTotal.ToString();
                           
                            apiResponse.dp1DTO.grossProductionGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;

                        case "revenuePerPatientGross": 
                            apiResponse.dp1DTO.revenuePerPatientGross = item.total.ToString();
                            apiResponse.dp1DTO.prevRevenuePerPatientGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.revenuePerPatientGrossGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "multiPairPct":
                            apiResponse.dp1DTO.multiPairPct = item.total.ToString();
                            apiResponse.dp1DTO.prevMultiPairPct = item.prevTotal.ToString();
                            apiResponse.dp1DTO.multiPairPctGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "conversionRate":
                            apiResponse.dp1DTO.conversionRate = item.total.ToString();
                            apiResponse.dp1DTO.prevConversionRate = item.prevTotal.ToString();
                            apiResponse.dp1DTO.conversionRateGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "fundusScreeningsCR":
                            apiResponse.dp1DTO.fundusScreeningsCR = item.total.ToString();
                            apiResponse.dp1DTO.prevFundusScreeningsCR = item.prevTotal.ToString();
                            apiResponse.dp1DTO.fundusScreeningsCRGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "arCaptureRate":
                            apiResponse.dp1DTO.arCaptureRate = item.total.ToString();
                            apiResponse.dp1DTO.prevArCaptureRate = item.prevTotal.ToString();
                            apiResponse.dp1DTO.arCaptureRateGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "existingPatients":
                            apiResponse.dp1DTO.existingPatients = item.total.ToString();
                            apiResponse.dp1DTO.prevExistingPatients = item.prevTotal.ToString();
                            apiResponse.dp1DTO.existingPatientsGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "newPatients":
                            apiResponse.dp1DTO.newPatients = item.total.ToString();
                            apiResponse.dp1DTO.prevNewPatients = item.prevTotal.ToString();
                            apiResponse.dp1DTO.newPatientsGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                       
                        case "clfittingscurrencyGross":
                            apiResponse.dp1DTO.clfittingscurrencyGross = item.total.ToString();
                            apiResponse.dp1DTO.prevClfittingscurrencyGross = item.prevTotal.ToString(); 
                            apiResponse.dp1DTO.clfittingscurrencyGrossGrowth = CalculateGrowth(item.total, item.prevTotal); break;
                        case "clmultifocalGross":
                            apiResponse.dp1DTO.clmultifocalGross = item.total.ToString(); break;
                        case "contactLensCaptureRate":
                            apiResponse.dp1DTO.contactLensCaptureRate = item.total.ToString();
                            apiResponse.dp1DTO.prevContactLensCaptureRate = item.prevTotal.ToString();
                            apiResponse.dp1DTO.contactLensCaptureRateGrowth = CalculateGrowth(item.total, item.prevTotal);


                            break;
                        case "clAnnualSupplyPct":
                            apiResponse.dp1DTO.clAnnualSupplyPct = item.total.ToString();
                            apiResponse.dp1DTO.prevClAnnualSupplyPct = item.prevTotal.ToString(); 
                            apiResponse.dp1DTO.ClAnnualSupplyPctGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "contactLensFittings":
                            apiResponse.dp1DTO.contactLensFittings = item.total.ToString(); 
                            apiResponse.dp1DTO.prevContactLensFittings = item.prevTotal.ToString();
                            apiResponse.dp1DTO.contactLensFittingsGrowth = CalculateGrowth(item.total, item.prevTotal); break;
                        case "clUnits":
                            apiResponse.dp1DTO.clUnits = item.total.ToString();
                            apiResponse.dp1DTO.prevClUnits = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clUnitsGrowth = CalculateGrowth(item.total, item.prevTotal); break;

                            
                         case "contactLensDollarsGrossUnlocked":
                            apiResponse.dp1DTO.contactLensDollarsGrossUnlocked = item.total.ToString();
                            apiResponse.dp1DTO.prevContactLensDollarsGrossUnlocked = item.prevTotal.ToString();
                            apiResponse.dp1DTO.contactLensDollarsGrossUnlockedGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "contactLensDollarsGross":
                            apiResponse.dp1DTO.contactLensDollarsGross = item.total.ToString(); 
                            apiResponse.dp1DTO.prevContactLensDollarsGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.contactLensDollarsGrossGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "refractions":
                            apiResponse.dp1DTO.refractions = item.total.ToString(); 
                            apiResponse.dp1DTO.prevRefractions = item.prevTotal.ToString(); 
                            apiResponse.dp1DTO.refractionsGrowth = CalculateGrowth(item.total, item.prevTotal); break;
                        case "refractionsGross":
                            apiResponse.dp1DTO.refractionsGross = item.total.ToString(); break;
                        case "cldailyGross":
                            apiResponse.dp1DTO.clDailyGross = item.total.ToString(); 
                            apiResponse.dp1DTO.prevClDailyGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clDailyGrossGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;

                        case "opticalDollarsGross": apiResponse.dp1DTO.opticalDollarsGross = item.total.ToString(); break;
                        case "officeVisitsDollarsGross": apiResponse.dp1DTO.officeVisitsDollarsGross = item.total.ToString(); break;
                        case "ancillaryGross": apiResponse.dp1DTO.ancillaryGross = item.total.ToString(); break;
                        case "medicalDollarsGross": apiResponse.dp1DTO.medicalDollarsGross = item.total.ToString(); break;
                        case "totalCaptureRate": apiResponse.dp1DTO.totalCaptureRate = item.total.ToString(); break;
                        case "paymentsPerPatient": apiResponse.dp1DTO.paymentsPerPatient = item.total.ToString(); break;
                        case "paymentsPerRefraction": apiResponse.dp1DTO.paymentsPerRefraction = item.total.ToString(); break;
                        case "payment":
                            apiResponse.dp1DTO.payment = item.total.ToString();
                            apiResponse.dp1DTO.prevPayment = item.prevTotal.ToString();
                            apiResponse.dp1DTO.paymentGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "patientPayments":
                            apiResponse.dp1DTO.patientPayments = item.total.ToString();
                            apiResponse.dp1DTO.prevPatientPayments = item.prevTotal.ToString();
                            apiResponse.dp1DTO.patientPaymentsGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "revenuePerRefractionGross": apiResponse.dp1DTO.revenuePerRefractionGross = item.total.ToString(); break;
                        case "production": apiResponse.dp1DTO.production = item.total.ToString(); break;
                        case "frameCaptureRate": apiResponse.dp1DTO.frameCaptureRate = item.total.ToString(); break;
                        case "revenuePerEyewearPairGross": 
                            apiResponse.dp1DTO.revenuePerEyewearPairGross = item.total.ToString();
                            apiResponse.dp1DTO.prevRevenuePerEyewearPairGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.revenuePerEyewearPairGrossGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "opticalCaptureRate": 
                            apiResponse.dp1DTO.opticalCaptureRate = item.total.ToString();
                            apiResponse.dp1DTO.prevOpticalCaptureRate = item.prevTotal.ToString();
                            apiResponse.dp1DTO.opticalCaptureRateGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "revenuePerMedicalPatient": apiResponse.dp1DTO.revenuePerMedicalPatient = item.total.ToString(); break;

                        case "photochromatic": apiResponse.dp1DTO.photochromatic = item.total.ToString(); break;
                        case "photochromaticCR": apiResponse.dp1DTO.photochromaticCR = item.total.ToString(); break;

                        case "lensCount": apiResponse.dp1DTO.lensCount = item.total.ToString(); break;


                        default:
                            Logger.log("Not Found:" + item.key.ToString());

                            break;
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine("Error Mapping to MapToExportToCSV. ex:" + ex.ToString());
                Logger.log("Error Mapping to MapToExportToCSV. ex:" + ex.ToString());
                throw;
            }
            return apiResponse;
        }

        private static APIWebsiteResponseModelDTO MapToExportToCSVJobson(APIWebsiteResponseModelDTO apiResponse, NationalTrendDTO nationalTrendDTO)
        {
            try
            { 
                //break;
                foreach (var item in apiResponse.metrics)
                {
                    switch (item.key)
                    { 
                        
                        case "cldailypct":
                            apiResponse.dp1DTO.cldailypct = item.total.ToString();
                            apiResponse.dp1DTO.prevCldailypct = item.prevTotal.ToString();
                            apiResponse.dp1DTO.cldailypctGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "cltwoweekpct":
                            apiResponse.dp1DTO.cltwoweekpct = item.total.ToString();
                            apiResponse.dp1DTO.prevCltwoweekpct = item.prevTotal.ToString();
                            apiResponse.dp1DTO.cltwoweekpctGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "clmonthlypct":
                            apiResponse.dp1DTO.clmonthlypct = item.total.ToString();
                            apiResponse.dp1DTO.prevClmonthlypct = item.prevTotal.ToString();
                            apiResponse.dp1DTO.clmonthlypctGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break; //clAnnualSupplyPct
                        case "clAnnualSupply":
                            apiResponse.dp1DTO.clAnnualSupply = item.total.ToString();
                            apiResponse.dp1DTO.prevClAnnualSupply = item.prevTotal.ToString();
                            apiResponse.dp1DTO.ClAnnualSupplyGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "clAnnualSupplyPct":
                            apiResponse.dp1DTO.clAnnualSupplyPct = item.total.ToString();
                            apiResponse.dp1DTO.prevClAnnualSupplyPct = item.prevTotal.ToString();
                            apiResponse.dp1DTO.ClAnnualSupplyPctGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "revenuePerCLFittingGross":
                            apiResponse.dp1DTO.revenuePerCLFittingGross = item.total.ToString();
                            apiResponse.dp1DTO.prevRevenuePerCLFittingGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.revenuePerCLFittingGrossGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;

                        case "contactLensCaptureRate":
                            apiResponse.dp1DTO.contactLensCaptureRate = item.total.ToString();
                            apiResponse.dp1DTO.prevContactLensCaptureRate = item.prevTotal.ToString();
                            apiResponse.dp1DTO.contactLensCaptureRateGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "openLocationPct":
                            apiResponse.dp1DTO.openLocationPct = item.total.ToString();
                            apiResponse.dp1DTO.previousOpenLocationPct = item.prevTotal.ToString();
                            apiResponse.dp1DTO.openLocationPctGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "refractions":
                            apiResponse.dp1DTO.refractions = item.total.ToString();
                            apiResponse.dp1DTO.prevRefractions = item.prevTotal.ToString();
                            
                            apiResponse.dp1DTO.refractionsGrowth = CalculateGrowth(item.total,item.prevTotal);
                            nationalTrendDTO.totalRefractions += item.total;

                            break;
                        case "refractionsGross":
                            apiResponse.dp1DTO.refractionsGross = item.total.ToString();
                            apiResponse.dp1DTO.prevRefractionsGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.refractionsGrossGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "spectacleLensSalesGross":
                            apiResponse.dp1DTO.spectacleLensSalesGross = item.total.ToString();
                            apiResponse.dp1DTO.prevSpectacleLensSalesGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.spectacleLensSalesGrossGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "framesDollarsGross":
                            apiResponse.dp1DTO.framesDollarsGross = item.total.ToString();
                            apiResponse.dp1DTO.prevFramesDollarsGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.framesDollarsGrossGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "grossProduction":
                            apiResponse.dp1DTO.grossProduction = item.total.ToString();
                            apiResponse.dp1DTO.prevGrossProduction = item.prevTotal.ToString();
                            nationalTrendDTO.totalGrossProduction += item.total;
                            apiResponse.dp1DTO.grossProductionGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "contactLensDollarsGross":
                            apiResponse.dp1DTO.contactLensDollarsGross = item.total.ToString();
                            apiResponse.dp1DTO.prevContactLensDollarsGross = item.prevTotal.ToString();
                            apiResponse.dp1DTO.contactLensDollarsGrossGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "frameUnits":
                            apiResponse.dp1DTO.frameUnits = item.total.ToString();
                            apiResponse.dp1DTO.prevFrameUnits = item.prevTotal.ToString();
                            nationalTrendDTO.totalFrameUnits += item.total;
                            apiResponse.dp1DTO.frameUnitsGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "clCount":
                            apiResponse.dp1DTO.clCount = item.total.ToString();
                            apiResponse.dp1DTO.prevClCount = item.prevTotal.ToString();
                            nationalTrendDTO.totalClCount += item.total;
                            apiResponse.dp1DTO.clCountGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;
                        case "spectacleLensUnits":
                            apiResponse.dp1DTO.spectacleLensUnits = item.total.ToString();
                            apiResponse.dp1DTO.prevSpectacleLensUnits = item.prevTotal.ToString();
                            apiResponse.dp1DTO.spectacleLensUnitsGrowth = CalculateGrowth(item.total, item.prevTotal);

                            break;
                        case "lensCount":
                            apiResponse.dp1DTO.lensCount = item.total.ToString();
                            apiResponse.dp1DTO.prevLensCount = item.prevTotal.ToString();
                            nationalTrendDTO.totalLensCount += item.total;

                            apiResponse.dp1DTO.lensCountGrowth = CalculateGrowth(item.total, item.prevTotal);
                            break;

                        default:
                            Logger.log("Not Found:" + item.key.ToString());

                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Mapping to MapToExportToCSV. ex:" + ex.ToString());
                Logger.log("Error Mapping to MapToExportToCSV. ex:" + ex.ToString());
                throw;
            }
            return apiResponse;
        }

        private static decimal CalculateGrowth(decimal total, decimal prevTotal)
        {
            decimal current = total;
            decimal previous = prevTotal;
            if (previous == 0)
            {
                return  0;
            }
            else
            {
                return ((current - previous) / previous) * 100;
            }
        }

        private static List<APIWebsiteResponseModelDTO> ParseJSONResponse(string JSONResponse) {
            JArray responseJsonArray = JArray.Parse(JSONResponse);
            List<APIWebsiteResponseModelDTO> responseList = new List<APIWebsiteResponseModelDTO>();
            try {
                foreach (var item in responseJsonArray) {

                    APIWebsiteResponseModelDTO responseToAdd = new APIWebsiteResponseModelDTO();
                    responseToAdd.practiceId = (string)item["practiceId"];
                    responseToAdd.practiceName = (string)item["practiceName"];
                    responseToAdd.locationId = (string)item["locationId"];
                    responseToAdd.locationName = (string)item["locationName"];
                    responseToAdd.employeeId = (string)item["employeeId"];
                    responseToAdd.employeeName = (string)item["employeeName"];
                    responseToAdd.insuranceId = (string)item["insuranceId"];
                    responseToAdd.insuranceName = (string)item["insuranceName"];
                    JArray metricsArray = (JArray)item["metrics"];
                    foreach (var metric in metricsArray) {
                        MetricsDto metricToAdd = new MetricsDto();
                        metricToAdd.key = (string)metric["key"];
                        metricToAdd.dataType = (string)metric["dataType"];
                        metricToAdd.label = (string)metric["label"];
                        metricToAdd.tooltip = (string)metric["tooltip"];
                        metricToAdd.trend = (int)metric["trend"];
                        metricToAdd.verifeye = (int)metric["verifeye"];
                        metricToAdd.featureId = (int)metric["featureId"];
                        metricToAdd.currencyType = (string)metric["currencyType"];
                        JArray dataArray = (JArray)metric["data"];
                        foreach (var data in dataArray) {
                            DataDTO dataToAdd = new DataDTO();
                            dataToAdd.data = (decimal)data["data"];
                            dataToAdd.label = (string)data["label"];
                            metricToAdd.data.Add(dataToAdd);
                        }
                        JArray lineItemArray = (JArray)metric["lineItems"];
                        foreach (var lineItem in lineItemArray) {
                            try {
                                LineItems lineItemToAdd = new LineItems();
                                lineItemToAdd.patientIdInteger = (int)lineItem["patientId"];
                                metricToAdd.lineItems.Add(lineItemToAdd);
                            } catch (Exception) {
                                //Ignore lineitems with issues
                            }

                        }
                        metricToAdd.total = (decimal)metric["total"];
                        metricToAdd.prevTotal = (decimal)metric["prevTotal"];
                        responseToAdd.metrics.Add(metricToAdd);
                    }
                    responseList.Add(responseToAdd);

                }
            } catch (Exception ex) {
                Console.WriteLine("Error Parsing Json. ex:" + ex.ToString());
            }
            return responseList;
        }

        //REGION TABLES CREATED AND UPDATED - REFACTOR WHEN USED FOR NEXT PROJECT
        private static string GetRegion(string state)
        {

            string region = "";
            switch (state)
            {
                case "NC": region = "South"; break;
                case "FL": region = "South"; break;
                case "TX": region = "South"; break;
                case "TN": region = "South"; break;
                case "KY": region = "South"; break;
                case "GA": region = "South"; break;
                case "OK": region = "South"; break;
                case "MS": region = "South"; break;
                case "LA": region = "South"; break;
                case "SC": region = "South"; break;
                case "VA": region = "South"; break;
                case "CO": region = "West"; break;
                case "UT": region = "West"; break;
                case "HI": region = "West"; break;
                case "ID": region = "West"; break;
                case "CA": region = "West"; break;
                case "OR": region = "West"; break;
                case "WA": region = "West"; break;
                case "AZ": region = "West"; break;
                case "AK": region = "West"; break;
                case "NV": region = "West"; break;
                case "IL": region = "MidWest"; break;
                case "MO": region = "MidWest"; break;
                case "MN": region = "MidWest"; break;
                case "IA": region = "MidWest"; break;
                case "ND": region = "MidWest"; break;
                case "OH": region = "MidWest"; break;
                case "MI": region = "MidWest"; break;
                case "WI": region = "MidWest"; break;
                case "IN": region = "MidWest"; break;
                case "KS": region = "MidWest"; break;
                case "NE": region = "MidWest"; break;
                case "CT": region = "NorthEast"; break;
                case "PA": region = "NorthEast"; break;
                case "NY": region = "NorthEast"; break;
                case "MD": region = "NorthEast"; break;
                case "NJ": region = "NorthEast"; break;
                case "MA": region = "NorthEast"; break;
                case "DC": region = "NorthEast"; break;
                case "NH": region = "NorthEast"; break;


                default:
                    region = ""; break;

            }

            return region;
        }

    }
}
