﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon
{
    public class PeccaExportDTO
    {
        public string clmultifocalPrevious { get; set; } = String.Empty;

        public string clmultifocalGrowth { get; set; } = String.Empty;

        public string clmultifocalpct { get; set; } = String.Empty;
        public string clmultifocal { get; set; } = String.Empty;
        public string revenuePerCLFittingGross { get; set; } = String.Empty;
    }
}
