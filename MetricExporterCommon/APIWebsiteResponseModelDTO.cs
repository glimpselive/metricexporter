﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon {
    public class APIWebsiteResponseModelDTO {
        public DateTime startDate { get; set; } = DateTime.MinValue;
        public DateTime endDate { get; set; } = DateTime.MinValue;
        public string practiceId = String.Empty;

        public string practiceName = String.Empty;

        public string locationId = String.Empty;

        public string locationName = String.Empty;

        public string employeeId = String.Empty;

        public string employeeName = String.Empty;
        public string insuranceId { get; set; } = String.Empty;
        public string insuranceName { get; set; } = String.Empty;
        public KpiGrossDTO KpiGross { get; set; } = new KpiGrossDTO();
        public TayeDTO tayeDTO { get; set; } = new TayeDTO();
        public PeccaExportDTO peccaExportDTO { get; set; } = new PeccaExportDTO();
        public DP1DTO dp1DTO { get; set; } = new DP1DTO();

        public List<MetricsDto> metrics = new List<MetricsDto>();
        public ivankaLocationDTO location;
    }
}
