﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon {
    public class MetricsDto {

        public string key;

        public string dataType;

        public string label;

        public string tooltip;

        public int trend = 1;

        public int verifeye;

        public int featureId;

        public string currencyType = String.Empty;

        public List<DataDTO> data = new List<DataDTO>();

        public decimal total;

        public List<DataDTO> prev = new List<DataDTO>();

        public decimal prevTotal;

        public List<LineItems> lineItems = new List<LineItems>();
    }
}
