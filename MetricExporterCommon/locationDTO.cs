﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon
{
    public class ivankaLocationDTO {
        public List<APIWebsiteResponseModelDTO> apiResponse = new List<APIWebsiteResponseModelDTO>();
        public List<APIWebsiteResponseModelDTO> apiResponsePracticeLevel = new List<APIWebsiteResponseModelDTO>();
        public DateTime startDate;
        public DateTime endDate;
        public DateTime maxBillingDate { get; set; } = DateTime.MinValue;
        public string timeframeMarketshare { get; set; } = String.Empty;
        public string timeframeBaseline { get; set; } = String.Empty;
        public string abbNumber { get; set; } = String.Empty;
        public string abbShipTo { get; set; } = String.Empty;
        public string locationName { get; set; } = String.Empty;
        public string state { get; set; } = String.Empty;
        public string city { get; set; } = String.Empty;
        public string zip { get; set; } = String.Empty;
        public string officePhone { get; set; } = String.Empty;
        public string emrName { get; set; } = String.Empty;
        public string accountType { get; set; } = String.Empty;
        public string url { get; set; } = String.Empty;
        public string country { get; set; } = String.Empty;
        public string program { get; set; } = String.Empty;
        public string group { get; set; } = String.Empty;
        public string address { get; set; } = String.Empty;
        public string address2 { get; set; } = String.Empty;
        public string region { get; set; } = String.Empty;
        public string practiceName { get; set; } = String.Empty;
        public string SFIDShipTo { get; set; } = String.Empty;
        public string SFIDBillTo { get; set; } = String.Empty;
        public string UCN { get; set; } = String.Empty;
        public string unified_customer_name { get; set; } = String.Empty;
        public string default_payer_number { get; set; } = String.Empty;
        public string default_payer_name { get; set; } = String.Empty;
        public int locationId { get; set; } = 0;
        public int practiceId { get; set; } = 0;
        public int active { get; set; } = 0;
        public bool legacy { get; set; } = false;
        public bool printPracticeLevel { get; set; } = false;

        public bool appendDPNToPID { get; set; } = false;
    }
}
