﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon
{
    public class DP1DTO
    {


        public string revenuePerMedicalPatientGross { get; set; } = String.Empty;
        public string cltoricGross { get; set; } = String.Empty;
        public string openLocationPct { get; set; } = String.Empty;
        public string previousOpenLocationPct { get; set; } = String.Empty;


        public string revenuePerCLFittingGross { get; set; } = String.Empty;
        public string prevRevenuePerCLFittingGross { get; set; } = String.Empty;
        public string distinctpatientid { get; set; } = String.Empty;
        public string prevDistinctpatientid { get; set; } = String.Empty;

        public string spectacleLensSalesGross { get; set; } = String.Empty;

        public string grossProduction { get; set; } = String.Empty;

        public string clmarketshare { get; set; } = String.Empty;
        public string alconClmarketshare { get; set; } = String.Empty;
        public string unityLensCount { get; set; } = String.Empty;
        public string paymentsByPatient { get; set; } = String.Empty;
        public string paymentsByInsurance { get; set; } = String.Empty;
        public string paymentsByInsuranceVSP { get; set; } = String.Empty;
        public string paymentsByInsuranceEyemed { get; set; } = String.Empty;
        public string cltoric { get; set; } = String.Empty;
        public string clmultifocal { get; set; } = String.Empty;
        public string clspherical { get; set; } = String.Empty;
        public string cldaily { get; set; } = String.Empty;
        public string cltwoweek { get; set; } = String.Empty;
        public string clmonthly { get; set; } = String.Empty;
        public string cloneweek { get; set; } = String.Empty;
        






        public string clmarketsharebaseline { get; set; } = String.Empty;
        public string clmarketsharebaselinePracticeLevel { get; set; } = String.Empty;
        public string clmarketsharePracticeLevel { get; set; } = String.Empty;
        
        public string clmarketshareTest { get; set; } = String.Empty;
        public string prevClmarketshare { get; set; } = String.Empty;
        public string prevAlconClmarketshare { get; set; } = String.Empty; 
            public string prevUnityLensCount { get; set; } = String.Empty;
        public string prevPaymentsByPatient { get; set; } = String.Empty;
        public string prevPaymentsByInsurance { get; set; } = String.Empty;
        public string prevPaymentsByInsuranceVSP { get; set; } = String.Empty;


        public string prevPaymentsByInsuranceEyemed { get; set; } = String.Empty;
        public string prevCldaily { get; set; } = String.Empty;
        public string prevCltoric { get; set; } = String.Empty;
        public string prevClmultifocal { get; set; } = String.Empty;
        public string prevClspherical { get; set; } = String.Empty;
        public string prevCltwoweek { get; set; } = String.Empty;
        public string prevClmonthly { get; set; } = String.Empty;
        public string prevCloneweek { get; set; } = String.Empty;
        public string prevClmarketshareTest { get; set; } = String.Empty;
        public string prevClmarketsharebaseline { get; set; } = String.Empty;
        public decimal clmarketshareGrowth { get; set; } = 0;
        public decimal alconClmarketshareGrowth { get; set; } = 0; 
            public decimal unityLensCountGrowth { get; set; } = 0;
        public decimal paymentsByPatientGrowth { get; set; } = 0;
        public decimal paymentsByInsuranceGrowth { get; set; } = 0;
        public decimal paymentsByInsuranceVSPGrowth { get; set; } = 0;


        public decimal paymentsByInsuranceEyemedGrowth { get; set; } = 0;
        public decimal cldailyGrowth { get; set; } = 0;
        public decimal cltoricGrowth { get; set; } = 0;
        public decimal clmultifocalGrowth { get; set; } = 0;
        public decimal clsphericalGrowth { get; set; } = 0;
        public decimal cltwoweekGrowth { get; set; } = 0;
        public decimal clmonthlyGrowth { get; set; } = 0;
        public decimal cloneweekGrowth { get; set; } = 0;

        public decimal clmarketshareTestGrowth { get; set; } = 0;
        public decimal clmarketsharebaselineGrowth { get; set; } = 0;


        public string contactLensFittings { get; set; } = String.Empty;
        public string prevContactLensFittings { get; set; } = String.Empty;
        
        public string contactLensDollarsGross { get; set; } = String.Empty;
        public string contactLensDollarsGrossUnlocked { get; set; } = String.Empty;
        
        public string refractionsGross { get; set; } = String.Empty;
        public string refractions { get; set; } = String.Empty;
        public string prevRefractions { get; set; } = String.Empty;

        public string clDailyGross { get; set; } = String.Empty;
        public string prevClDailyGross { get; set; } = String.Empty;

        public string cldailypct { get; set; } = String.Empty;
        public string prevCldailypct { get; set; } = String.Empty;

        public string cltwoweekpct { get; set; } = String.Empty;
        public string prevCltwoweekpct { get; set; } = String.Empty;
        public string clmonthlypct { get; set; } = String.Empty;
        public string prevClmonthlypct { get; set; } = String.Empty;


        public string frameUnits { get; set; } = String.Empty;
        public string prevFrameUnits { get; set; } = String.Empty;

        public string clCount { get; set; } = String.Empty;
        public string prevClCount { get; set; } = String.Empty;

        public string spectacleLensUnits { get; set; } = String.Empty;
        public string specialty { get; set; } = String.Empty;
        public string clmaterialscurrencyGross { get; set; } = String.Empty;
        public string prevClmaterialscurrencyGross { get; set; } = String.Empty; 
        public string singleVision { get; set; } = String.Empty;
        public string bifocal { get; set; } = String.Empty;
        public string trifocal { get; set; } = String.Empty;
        public string progressive { get; set; } = String.Empty;
        public string premiumProgressive { get; set; } = String.Empty;
        public string premiumSingle { get; set; } = String.Empty;
        public string antireflective { get; set; } = String.Empty;
        public string polarization { get; set; } = String.Empty;
        public string clspecialty { get; set; } = String.Empty;

        public string prevSpectacleLensUnits { get; set; } = String.Empty;

        public string lensCount { get; set; } = String.Empty;
        public string prevLensCount { get; set; } = String.Empty;

       

        public string revenuePerPatientGross { get; set; } = String.Empty;

        public string prevRefractionsGross { get; set; } = String.Empty;
        public string prevSpectacleLensSalesGross { get; set; } = String.Empty;
        public string prevFramesDollarsGross { get; set; } = String.Empty;
        public string prevGrossProduction { get; set; } = String.Empty; 

        public string prevContactLensDollarsGrossUnlocked { get; set; } = String.Empty;
        public string prevContactLensDollarsGross { get; set; } = String.Empty;
        public decimal clmaterialscurrencyGrossGrowth { get; set; } = 0; 
        public decimal revenuePerCLFittingGrossGrowth { get; set; } = 0;
        public decimal distinctpatientidGrowth { get; set; } = 0;
        public decimal grossProductionGrowth { get; set; } = 0;
        public decimal lensCountGrowth { get; set; } = 0;
        public decimal spectacleLensUnitsGrowth { get; set; } = 0;
        public decimal clDailyGrossGrowth { get; set; } = 0;
        public decimal cldailypctGrowth { get; set; } = 0;
        public decimal cltwoweekpctGrowth { get; set; } = 0;
        public decimal clmonthlypctGrowth { get; set; } = 0;

        public decimal clCountGrowth { get; set; } = 0;
        public decimal frameUnitsGrowth { get; set; } = 0; 

        public decimal contactLensDollarsGrossUnlockedGrowth { get; set; } = 0;
        public decimal contactLensDollarsGrossGrowth { get; set; } = 0;
        public decimal framesDollarsGrossGrowth { get; set; } = 0;
        public decimal spectacleLensSalesGrossGrowth { get; set; } = 0;
        public decimal contactLensFittingsGrowth { get; set; } = 0;
        public decimal refractionsGrossGrowth { get; set; } = 0;
        public decimal refractionsGrowth { get; set; } = 0;
        public decimal contactLensCaptureRateGrowth { get; set; } = 0;
        public decimal opticalCaptureRateGrowth { get; set; } = 0;
        public decimal openLocationPctGrowth { get; set; } = 0;
        public decimal ClAnnualSupplyPctGrowth { get; set; } = 0;
        public decimal clUnitsGrowth { get; set; } = 0;
        public decimal ClAnnualSupplyGrowth { get; set; } = 0;
        public decimal clfittingscurrencyGrossGrowth { get; set; } = 0;
        public decimal revenuePerPatientGrossGrowth { get; set; } = 0; 
        //TOTALS JOBSON
        public string totalRefractions { get; set; } = String.Empty;
        public string totalClCount { get; set; } = String.Empty;
        public string totalFrameUnits { get; set; } = String.Empty;
        public string totalLensCount { get; set; } = String.Empty;
        public string totalGrossProduction { get; set; } = String.Empty;
        public string contactLensCaptureRate { get; set; } = String.Empty;
        public string prevContactLensCaptureRate { get; set; } = String.Empty;
        public string prevOpticalCaptureRate { get; set; } = String.Empty;
        public string clAnnualSupplyPct { get; set; } = String.Empty;
        public string clAnnualSupply { get; set; } = String.Empty;
        public string clUnits { get; set; } = String.Empty;

        public string prevClAnnualSupplyPct { get; set; } = String.Empty;
        public string prevClAnnualSupply { get; set; } = String.Empty;
        public string prevClUnits { get; set; } = String.Empty; 
        public string clmultifocalGross { get; set; } = String.Empty;

        public string clfittingscurrencyGross { get; set; } = String.Empty;
        public string prevClfittingscurrencyGross { get; set; } = String.Empty;

        public string conversionRate { get; set; } = String.Empty;
        public string prevConversionRate { get; set; } = String.Empty;
        public decimal conversionRateGrowth { get; set; } = 0;
        
        public string fundusScreeningsCR { get; set; } = String.Empty;
        public string prevFundusScreeningsCR { get; set; } = String.Empty;
        public decimal fundusScreeningsCRGrowth { get; set; } = 0;

        public string arCaptureRate { get; set; } = String.Empty;
        public string prevArCaptureRate { get; set; } = String.Empty;
        public decimal arCaptureRateGrowth { get; set; } = 0;

        public string existingPatients { get; set; } = String.Empty;
        public string prevExistingPatients { get; set; } = String.Empty;
        public decimal existingPatientsGrowth { get; set; } = 0;

        public string newPatients { get; set; } = String.Empty;
        public string prevNewPatients { get; set; } = String.Empty;
        public decimal newPatientsGrowth { get; set; } = 0;

        public string revenuePerEyewearPairGross { get; set; } = String.Empty;
        public string prevRevenuePerEyewearPairGross { get; set; } = String.Empty;
        public decimal revenuePerEyewearPairGrossGrowth { get; set; } = 0;

        public string multiPairPct { get; set; } = String.Empty;
        public string prevMultiPairPct { get; set; } = String.Empty;
        public decimal multiPairPctGrowth { get; set; } = 0;

        public string prevPayment { get; set; } = String.Empty;
        public decimal paymentGrowth { get; set; } = 0;
       
        public string patientPayments { get; set; } = String.Empty;
        public string prevPatientPayments { get; set; } = String.Empty;
        public decimal patientPaymentsGrowth { get; set; } = 0;


        public string prevRevenuePerPatientGross { get; set; } = String.Empty;
        public string opticalDollarsGross { get; set; } = String.Empty;
        public string photochromatic { get; set; } = String.Empty;
        public string photochromaticCR { get; set; } = String.Empty;
        public string officeVisitsDollarsGross { get; set; } = String.Empty;
        public string ancillaryGross { get; set; } = String.Empty;
        public string medicalDollarsGross { get; set; } = String.Empty;
        public string totalCaptureRate { get; set; } = String.Empty;
        public string paymentsPerPatient { get; set; } = String.Empty;
        public string paymentsPerRefraction { get; set; } = String.Empty;
        public string payment { get; set; } = String.Empty;
        public string revenuePerRefractionGross { get; set; } = String.Empty;
        public string production { get; set; } = String.Empty;
        public string framesDollarsGross { get; set; } = String.Empty;
        public string frameCaptureRate { get; set; } = String.Empty;
       
        public string opticalCaptureRate { get; set; } = String.Empty;
        public string revenuePerMedicalPatient { get; set; } = String.Empty;

    }
}
