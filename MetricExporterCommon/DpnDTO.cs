﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricExporterCommon
{
    public class DpnDTO
    {
        public string default_payer_number { get; set; } = String.Empty;
        public bool containsParentLevel { get; set; } = false;
    }
}
